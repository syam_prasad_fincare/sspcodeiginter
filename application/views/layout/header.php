<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title><?=$title ?></title>
	<meta content="text/html;charset=utf-8" http-equiv="Content-Type">
	<meta content="utf-8" http-equiv="encoding">
	
	<?php include('header_js_css.php');?>
</head>

<body>
	
	
	<div class="container-fluid">
		<div class="row header">
				<div class="col-md-6 ">
					<div class="fincare-logo">
						<img src="<?=$this->config->item('images_path')?>fincare-logo.png" alt="" width="auto" height="100%"> </div>	
				
				</div>	
				<?php 	if (isset($this->session->loginStatus) ) {?>
				<nav class="navbar navbar-expand-sm col-md-6" >
					<!-- <div class="container-fluid">
						<img src="<?=$this->config->item('images_path')?>image_logo.png" style="height:30px;" alt="">
					</div> -->
					<ul class="navbar-nav ml-auto mt-2 mt-lg-0" id="dropdown_right">

						<li class="nav-item dropdown">
							<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								<i style="font-size:1.4em;" class="fa fa-user-circle"></i> Welcome! <mark style="color:black;font-weight:600;background-color: white;"><?php echo $this->session->username;?></mark>
							</a>
						<div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
							
							<div class="dropdown-divider"></div>
								<a class="dropdown-item" style="color:#A8060A;" href="<?php echo base_url('home/logout');?>">Logout <i class="fa fa-sign-out"></i></a>
							</div>
						</li>
					</ul>
				</nav>
			<?php }?>

		</div>
		<!-- <div class="header">
			<div class="fincare-logo">
				<img src="<?=$this->config->item('images_path')?>fincare-logo.png" alt="" width="auto" height="100%"> </div>	
		
		</div>		
	 -->
	 <div class="loader " id="show_loader">
   
       <img class="loading-image" src="<?=$this->config->item('images_path')?>demo_wait.gif" style="height:100px;width:100px;" alt="loading..">
   
	</div>
	 

	
		

