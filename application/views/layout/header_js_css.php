<link rel="stylesheet" href="<?=$this->config->item('css_path')?>bootstrap.min.css">
<link href="https://fonts.googleapis.com/css?family=Roboto:400,700&display=swap" rel="stylesheet">

<link rel="stylesheet" href="<?=$this->config->item('css_path')?>ssp_login_changed.css">
<?php if($photo ==TRUE){?>
<link rel="stylesheet" href="<?=$this->config->item('css_path')?>/style_photo.css">  
<?php } ?>
<!-- <link rel="stylesheet" href="assets/css/boot4/style.css">   -->
<link rel="stylesheet" type="text/css" href="<?=$this->config->item('css_path')?>jquery-ui.css">

<script src="<?=$this->config->item('js_path')?>jquery.min.js"></script>
<!-- <script src="https://unpkg.com/sweetalert2@7.19.3/dist/sweetalert2.all.js"></script> -->
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script> -->
<script src="<?=$this->config->item('js_path')?>popper.min.js"></script>
<script src="<?=$this->config->item('js_path')?>bootstrap.min.js"></script>
<script src="<?=$this->config->item('js_path')?>jquery.cookie.js"></script>	


<script type="text/javascript" src="<?=$this->config->item('js_path')?>script_user.js" ></script>
<script type="text/javascript" src="<?=$this->config->item('js_path')?>aes.js"></script>
<script type="text/javascript" src="<?=$this->config->item('js_path')?>aes-json-format.js"></script>
<script src="<?=$this->config->item('js_path')?>jquery-ui-custom.min.js" type="text/javascript"></script>







 	<script>
        $(function($) {
            // this script needs to be loaded on every page where an ajax POST may happen
            $.ajaxSetup({
                data: {
                    'fincarebank': $.cookie('bankfincare') 
                }
            });
        });

    </script>
    
    <style type="text/css">

	::selection { background-color: #E13300; color: white; }
	::-moz-selection { background-color: #E13300; color: white; }

	/*body {
		background-color: #fff;
		margin: 40px;
		font: 13px/20px normal Helvetica, Arial, sans-serif;
		color: #4F5155;
	}*/

	a {
		color: #003399;
		background-color: transparent;
		font-weight: normal;
	}

	h1 {
		color: #444;
		background-color: transparent;
		border-bottom: 1px solid #D0D0D0;
		font-size: 19px;
		font-weight: normal;
		margin: 0 0 14px 0;
		padding: 14px 15px 10px 15px;
	}

	code {
		font-family: Consolas, Monaco, Courier New, Courier, monospace;
		font-size: 12px;
		background-color: #f9f9f9;
		border: 1px solid #D0D0D0;
		color: #002166;
		display: block;
		margin: 14px 0 14px 0;
		padding: 12px 10px 12px 10px;
	}

	

	p.footer {
		text-align: right;
		font-size: 11px;
		border-top: 1px solid #D0D0D0;
		line-height: 32px;
		padding: 0 10px 0 10px;
		margin: 20px 0 0 0;
	}

	#container {
		margin: 0px;
		border: 1px solid #D0D0D0;
		box-shadow: 0 0 8px #D0D0D0;
	}
	</style>