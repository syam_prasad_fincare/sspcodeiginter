
<script src="<?=$this->config->item('js_path')?>custom.js"></script>
<script type="text/javascript" src="<?=$this->config->item('js_path')?>rollups/aes.js"></script>
<script type="text/javascript" src="<?=$this->config->item('js_path')?>rollups/pbkdf2.js"></script>
<script type="text/javascript" src="<?=$this->config->item('js_path')?>rollups/sha512.js"></script>
<script src="<?=$this->config->item('js_path')?>jquery.validate.min.js"></script>
<script src="<?=$this->config->item('js_path')?>additional-methods.min.js"></script>	
<script src="<?=$this->config->item('js_path')?>vendor.min.js"></script>
<script type="text/javascript">
	 	let site_url='<?=site_url('/');?>';
        let base_url='<?=base_url();?>';     
        let encryption_key = '<?= $this->config->item("aesencryption_key"); ?>';


</script>