<?php


if(! function_exists("pre_obj")) {

    /**
     * put object in pre tag
     *
     * @return bool
     */
    function pre_obj($obj,$is_die=null)
    {
		echo '<pre>';
		print_r($obj);
		echo '</pre>';
		if(is_null($is_die))
		{
			die;
		}
    }
}
if(! function_exists("pre_print")) {

    /**
     * put object in pre tag
     *
     * @return bool
     */
    function pre_print($obj,$is_die=null)
    {
        echo '<pre>';
        print_r($obj);
        echo '</pre>';
        // if(is_null($is_die))
        // {
        //     die;
        // }
    }
}

if(! function_exists("get_day_name")) {
	function get_day_name($date,$format) {
	
		$date = date('Y-m-d', strtotime($date));

		if($date == date('Y-m-d')) {
			$date = 'Today';
		} else if($date == date('Y-m-d',strtotime(date("Y-m-d")) - (24 * 60 * 60))) {
			$date = 'Yesterday';
		}else{
			$date = date($format, strtotime($date));
		}
		return $date;
	}
}

if(! function_exists("isEmpty")) {

    /**
     * put object in pre tag
     *
     * @return bool
     */
    function isEmpty($str = null){
        return empty($str) ? "":$str;
    }
}


if(! function_exists("pre_json_encode")) {

    /**
     * put object in pre tag
     *
     * @return bool
     */
    function pre_json_encode($obj,$is_die=null)
    {
		echo '<pre>';
		print_r(json_encode($obj));
		echo '</pre>';
		if(is_null($is_die))
		{
			die;
		}
    }
}


if(! function_exists("is_localhost_request")) {

    /**
     * put object in pre tag
     *
     * @return bool
     */
    function is_localhost_request()
    {
            $whitelist = array(
                '127.0.0.1',
                '::1'
            );

            if(!in_array($_SERVER['REMOTE_ADDR'], $whitelist)){
                   return FALSE;
            }else{
                return TRUE;
            }
    }

   
}


function is_empty($str ){
    return empty($str) ? "" : $str;
}


