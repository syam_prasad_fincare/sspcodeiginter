<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
defined('FILE_READ_MODE')  OR define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE')   OR define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE')  OR define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/
defined('FOPEN_READ')                           OR define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE')                     OR define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE')       OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE')  OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE')                   OR define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE')              OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT')            OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT')       OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
defined('EXIT_SUCCESS')        OR define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR')          OR define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG')         OR define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE')   OR define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS')  OR define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') OR define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT')     OR define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE')       OR define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN')      OR define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX')      OR define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code




/*
|--------------------------------------------------------------------------
| DATA CONSTANTS
|--------------------------------------------------------------------------
|
| These are constant like sms text,display messages etc.
|
*/
 //ssp

define('ApplicationID', '20');
define('AppUserID', 'SSP');
define('AppPassword', 'se56789p2wefgn');
define('APIKey', '2018120517075555');
define('API_Number', '10354_0_2');

define('Environment', 'D');
define('MAC_ID', '');
define('SessionID', '716e7fbd-a487-11e9-b92c-fa163eb56d8d');
define('captcha_expiry', 120);

//cib
define('ApplicationID1', '9');
define('AppUserID1', 'cibApp');
define('AppPassword1', 'AK45qwerty');
define('APIKey1', '2018120517071299');
define('API_Number1', '16838_1_3');

define('ApplicationID2', '1');
define('AppUserID2', 'Fincare');
define('AppPassword2', 'V2VsY29tZUAxMjM=');
define('APIKey2', '2018120517071234');
define('API_Number2', '10354_0_2');
//define('API_Number2', '10354_0_2');




// messages

define('successMsg', 'Verifying OTP');
define('resendOTPText', 'OTP has been resent to your mobile');
// // API URLS
// define('cbfdclose_api', 'https://uat-esb02.fincarebank.in/cbfdclose');
// define('depositedetails_api', 'https://uat-esb02.fincarebank.in/depositedetails');
// define('cb_imps_request_api', 'https://uat-esb02.fincarebank.in/cb_imps_request');
// define('CB_NEFT_api', 'https://uat-esb02.fincarebank.in/CB_NEFT');
// define('fundTransfer_api','https://uat-esb02.fincarebank.in/fundTransfer');

// define('cb_imps_instapay_api', 'https://uat-esb02.fincarebank.in/instpayimps');
// define('CB_NEFT_instapay_api', 'https://uat-esb02.fincarebank.in/instapayneft');
// define('CB_RTGS_instapay_api', 'https://uat-esb02.fincarebank.in/instapayrtgs');

// define('chqstausenq_api', 'https://uat-esb02.fincarebank.in/chqstausenq');
// define('chkrqst_api', 'https://uat-esb02.fincarebank.in/chkrqst');
// define('stopcheque_api', 'https://uat-esb02.fincarebank.in/stopchqpay');

// define('accountslist_api', 'https://uat-esb02.fincarebank.in/accountslist');
// define('ministatement_api', 'https://uat-esb02.fincarebank.in/ministatement');

// define('accountstatement_api', 'https://uat-esb02.fincarebank.in/acc_stat_request');
// define('depositslist_api', 'https://uat-esb02.fincarebank.in/depositlist');

// define('createdeposite_api', 'https://uat-esb02.fincarebank.in/createdeposite');
// define('lookupcif_api', 'https://uat-esb02.fincarebank.in/customer_details');
// define('depositstypelist_api', 'https://uat-esb02.fincarebank.in/cust_deposit_product_list');
// define('accountsenquiry_api', 'https://uat-esb02.fincarebank.in/ActInq');

// define('smssendotp_api', 'https://uat-esb02.fincarebank.in/SendOTPSMS');
// define('smsotpverify_api', 'https://uat-esb02.fincarebank.in/VerifyOTP');
// define('ifsccode_api', 'https://uat-esb02.fincarebank.in/IFSC_Code');
// define('dep_minmaxamount', 'https://uat-esb02.fincarebank.in/dep_minmaxamount');
// define('internal_ifsc_code', 'FSFB0000001');


// define('genericemail_api', 'https://uat-esb02.fincarebank.in/genricmail');

// define('loginCBApi_api', 'https://uat-esb02.fincarebank.in/loginCBApi');
// define('CBLoginAndUserClass', 'https://uat-esb02.fincarebank.in/CBLoginAndUserClass');
// // idam apis

define('CBLoginNewApi', 'https://uat-esb02.fincarebank.in/IBLogin');
//define('lookupbank_api', 'https://uat-esb02.fincarebank.in/getEmploymentDetails');
define('lookupbank_api', 'http://172.19.128.95:8280/getEmploymentDetails');


define('SendUserSMS_api', 'https://uat-esb02.fincarebank.in/SendUserSMS');
define('bulkupload', 'https://fisesb.fincarebank.com/disha/services/ws/secured/unt');
define('intrabenef_list', 'https://uat-esb02.fincarebank.in/intrabenef_list');
define('CBRtgsBeneficiaryList_api', 'https://uat-esb02.fincarebank.in/CBRtgsBeneficiaryList');
define('addbeneficiary_api', 'https://uat-esb02.fincarebank.in/addbenificary');
define('deletebeneficiary_api', 'https://uat-esb02.fincarebank.in/deletebenificiary');
// tds reports
define('tds_customersearch_api', 'https://uat-esb.fincarebank.in/restgateway/services/dms/fileprocess');



