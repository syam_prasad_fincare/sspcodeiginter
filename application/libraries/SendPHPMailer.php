<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH."/third_party/PHPMailer/class.smtp.php";
require_once APPPATH."/third_party/PHPMailer/class.phpmailer.php";
 
class SendPHPMailer extends PHPMailer {
    public function __construct() {
        parent::__construct();
    }
}
