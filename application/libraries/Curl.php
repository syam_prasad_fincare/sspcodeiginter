<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Curl 
{

    protected $CI;    
    public function __construct()
    {
        $this->CI =& get_instance();
        $this->CI->load->library('commonLibrary');
        $this->CI->load->database();
       // $this->CI->load->library('benchmark');
    }

    public function executeRequest($requestURL,$postData,$methodname, $parmetType,$returnType,$responseType=null){
    
        
    try{
        
            if($methodname!="xml"){
                $data_string = ($parmetType="json") ? json_encode($postData) : $postData; 
                $headers = ($parmetType=="json") ? array('Content-Type:application/json') : array('Content-Type:application/text');
                $curl = curl_init($requestURL);
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST,$methodname);
            }else{
                $data_string = $postData; 
                $headers = array(
                    "Content-type: text/xml",
                    "Content-length: " . strlen($postData),
                    "Connection: close",
                   
                );
                $curl = curl_init(); 
                curl_setopt($curl, CURLOPT_URL,$requestURL);
            }
            curl_setopt($curl,  CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl,  CURLOPT_POST, true);
            
            //curl_setopt($curl, Access-Control-Allow-Origin,'*');
            curl_setopt($curl,  CURLOPT_POSTFIELDS,$data_string);
            curl_setopt($curl,  CURLOPT_HTTPHEADER, $headers);
        
            $this->CI->benchmark->mark('code_start');

            // To turn off SSL
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
           //  echo "<pre/>";
           //    print_r($curl);
           //   echo $requestURL;
           //   print_r($headers);
           // print_r($data_string);
            
            $response = curl_exec($curl); 
            $returnCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
              // echo "<pre/>";
              // print_r($response);
              // print_r($returnCode);
               //die();
            $this->CI->benchmark->mark('code_end');

            $logData=array(
                'api_ref_id' => time(), 
                'api_q_id' => time(), 
                'api_request' => $data_string, 
                'req_url' => $requestURL,
                'api_request_date' => date('Y-m-d H:i:s'), 
                'api_response' => $response, 
                'api_response_date' => date('Y-m-d H:i:s'), 
                'created_by' =>  $this->CI->session->userID,
                'api_response_time'=> $this->CI->benchmark->elapsed_time('code_start', 'code_end')
            );

           //pre_obj($logData);
            $spRes = $this->CI->db->insert('crp_api_log',$logData);


           //$spRes =  $this->CI->commonlibrary->createProcedureParams('sp_maint_api_log', $logData, '');
            


            $returnCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
            if(curl_errno($curl)){
                $response = curl_error($curl);
            }else{
                curl_close($curl);
            }
            //echo "<pre/>";
               //print_r($returnCode);
            // pre_obj($logData);

            if($returnCode!=200)
                throw new Exception(json_encode(
                    array(
                        'status'=>'There was an error while processing request',
                        'code'  =>-1
                        )
                ));
           // echo "<pre/>";
             //pre_obj($responseType);
            if($responseType == 'soap')
            {

                $resDecode = json_decode($response);

                if(@$resDecode->status=="failed"){
                    $objResponse['status'] ='error';
                    $objResponse['message'] =$resDecode->error_message;
                }else{
                    
                    $xmlstr = preg_replace("/(<\/?)(\w+):([^>]*>)/", "$1$2$3", $response);
                    $objResponse=simplexml_load_string($xmlstr);


                }
                if($returnType=="json"){
                    return json_encode($objResponse);
                }else{
                    return $objResponse;
                }
            }
            else{

                if($returnType=="json"){
                    return json_encode($response);
                }else{
                    return $response;
                }

            }

  

    }    
    catch(Exception $e){

        if($returnType=="json"){
            return json_encode($e->getMessage());
        }else{
            return $e->getMessage();
        }        

    }   


}



}
