<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class CommonLibrary {

    protected $CI;

    public function __construct() {
        $this->CI = & get_instance();
        $this->CI->load->database();

        // unset($_SESSION);
        if (empty($this->CI->session->userdata('loginStatus'))) {
            //redirect('/', 'location');
        }

        $this->CI->encryption->initialize(
                array(
                    'cipher' => 'aes-256',
                    'mode' => 'cbc',
                    'key' => $this->CI->config->item('encryption_key')
                )
        );
    }

    // Function to setup procedur
    public function createProcedureParams($spname = null, $spdata = null, $spreturntype = null,$multiresult = false) {
        try {

            // Setup procedure array
            $proc_data = array();
            if (is_array($spdata) && sizeof($spdata) > 0) {
                foreach ($spdata as $key => $data) {
                    $proc_data[] = $this->CI->db->escape($data);
                }
            }

            // Call procedure
            $spcall = "CALL " . $spname . "(" . implode(",", $proc_data) . ")";
        
			if($multiresult){
				$results = $this->GetMultipleQueryResult($spcall);
			}else{
            	$results = $this->getResults($spcall);
			}

            // Return data based on return type
            if ($spreturntype == 'json')
                return(json_encode($results));
            else
                return $results;
        } catch (Exception $e) {

            $e->getMessage();
        }
    }

    // Function to fetch query result
    public function getResults($spcall = null) {

        // Run sp query
        // $this->CI->db->initialize();
		$query = $this->CI->db->query($spcall);
		

		if ($query->num_rows() > 0) {
            $results = $query->result_array();
		}
        // neeraj
        // $query->next_result();
        // $query->free_result();
        // vicky
        $conn = $this->CI->db->conn_id;
        do {
            if ($result = mysqli_store_result($conn)) {
                mysqli_free_result($result);
            }
        } while (mysqli_more_results($conn) && mysqli_next_result($conn));


        // $this->CI->db->reconnect();
        return $results ?? '';
	}
	

	public function GetMultipleQueryResult($queryString)
{
    if (empty($queryString)) {
                return false;
            }

    $index     = 0;
    $ResultSet = array();

    /* execute multi query */
    if (mysqli_multi_query($this->CI->db->conn_id, $queryString)) {
        do {
            if (false != $result = mysqli_store_result($this->CI->db->conn_id)) {
                $rowID = 0;
                while ($row = $result->fetch_assoc()) {
                    $ResultSet[$index][$rowID] = $row;
                    $rowID++;
                }
            }
            $index++;
        } while (mysqli_more_results($this->CI->db->conn_id) && mysqli_next_result($this->CI->db->conn_id));
    }

    return $ResultSet;
}

    // Function to create XML
    public function createXML($data = null) {

        $xml = '<resultset><details>';

        foreach ($data as $key => $xmldata) {
            $xml .= '<field name="' . $key . '">' . $xmldata . '</field>';
        }

        $xml .= '</details></resultset>';

        return $xml;
    }

    
    

    // Function to send OTP
    public function sendSMSOTP($otpdata = null) {

        $request = array(
            'APIAuthentication' => array(
                "ApplicationID" => ApplicationID1,
                "AppUserID" => AppUserID1,
                "AppPassword" => AppPassword1,
                "APIKey" => APIKey1,
                "API_Number" => API_Number1,
                "Environment" => Environment,
                "MAC_ID" => MAC_ID,
                "SessionID" => ""
            ),
            "Operation" => "OTPSMS",
            "mob" => $otpdata['mobileno']
        );

        $response_array = array();
        $url = smssendotp_api;
        $response = $this->CI->curl->executeRequest($url, $request, 'POST', 'json', '', '');


        //$responseData=$response->Response->Representations->Representation->Element;  

        $otpResponse = null;
        if (isset($response)) {

            $otpResponse = json_decode($response, true);
            $tempdata = array(
                'otpsmsid' => $otpResponse['ReferenceNumber'],
                'mobileno' => $otpdata['mobileno']
            );

            $this->CI->session->set_userdata($tempdata);
        }

        return $otpResponse;
    }



    // Function to send OTP
    public function sendUserOnBoardingSMS($otpdata = null) {

        $request = array(
            'APIAuthentication' => array(
                "ApplicationID" => ApplicationID1,
                "AppUserID" => AppUserID1,
                "AppPassword" => AppPassword1,
                "APIKey" => APIKey1,
                "API_Number" => API_Number1,
                "Environment" => Environment,
                "MAC_ID" => MAC_ID,
                "SessionID" => ""
            ),
            "Operation" => "OTPSMS",
            "mob" => $otpdata['mobileno']
        );
       
        $response_array = array();
        $url = SendUserSMS_api;
        $response = $this->CI->curl->executeRequest($url, $request, 'POST', 'json', '', '');

    }

    // Function to send OTP SMS
    public function verifySMSOTP($verifyOTP = null, $referenceNumber = null) {

        $request = array(
            'APIAuthentication' => array(
                "ApplicationID" => ApplicationID1,
                "AppUserID" => AppUserID1,
                "AppPassword" => AppPassword1,
                "APIKey" => APIKey1,
                "API_Number" => API_Number1,
                "Environment" => Environment,
                "MAC_ID" => MAC_ID,
                "SessionID" => ""
            ),
            "Operation" => "OTPVerification",
            "otp" => $verifyOTP,
            "ReferenceNumber" => $referenceNumber);
        $url = smsotpverify_api;
        $response = $this->CI->curl->executeRequest($url, $request, 'POST', 'json', '', '');

        //$responseData=$response->Response->Representations->Representation->Element;


        return (string) $response;
	}
	
	

     public function validateLogin($arr = []){
       
                 $responseArray= [];

        $request = [
            'APIAuthentication' => array(
                "ApplicationID" => ApplicationID,
                "AppUserID" => AppUserID,
                "AppPassword" => AppPassword,
                "APIKey" => APIKey,
                "API_Number" => API_Number2,
                "Environment" => Environment,
                "MAC_ID" => MAC_ID,
                "SessionID" => SessionID
            ),
            "Operation" => "IBLogin", 
            "userid" => $arr['userid'], 
            "password" => $arr['userpw']  
         ]; 
        
        $url = CBLoginNewApi;
        $response = $this->CI->curl->executeRequest($url, $request, 'POST', 'jsno', '', 'json');
        
        $response = json_decode($response,true);
        
        //$flag=false;
        // pre_obj($response);
        if($response['MRPC097']['STATUS'] == '0')
        {        
            $flag=true;
            $result['status'] = "Logged successfully";
            $result['code'] = 100;
        }else{
            $result['status'] = "Invalid User/Password";
            $result['code'] = -1;
            $flag=false;
        }
        if($flag)
        {
            
            $mobile=$response['MRPC097']['CIF']['BPH'];
            $otpDataInput = array(
                    'mobileno' => $mobile
                );
            if (is_localhost_request()) {
                $otpResponse['status'] = "Success";
                $otpResponse['ReferenceNumber'] = "817c02f9-85a6-423d-ae9d-927ead4f7273";
            } else {
                $otpResponse = $this->sendSMSOTP($otpDataInput);
            }
            if ((!isset($otpResponse['code']) && strtolower($otpResponse['status']) == "success") || (isset($otpResponse['code']) && empty($otpResponse['code']))) 
            {
                
                // //pre_obj($res);
                $tempdata = array(
                    'userID' =>$response['MRPC097']['CIF']['ACN'],  
                    'username' =>$response['MRPC097']['CIF']['NAM'],  
                    'mobileNo' =>$response['MRPC097']['CIF']['BPH'],  
                    'emailID' =>$response['MRPC097']['CIF']['EMAIL'],  

                );
               
                $this->CI->session->set_userdata($tempdata);

                array_push($responseArray, array('status' => successMsg, 'code' => 100));
                return $responseArray;

            } else {

                array_push($responseArray, $otpResponse);
                return $responseArray;
            }
        }
        else
        {
             array_push($responseArray,$result);
             return $responseArray;
        }
        
    }

   
    public function loginStepTwo() {

        // Server Side Validations
        $this->CI->form_validation->set_rules('otpValue', 'OTP Value', 'required', array('required' => 'Please enter valid OTP'));

        if ($this->CI->form_validation->run()) {

            $otpValue = $this->CI->input->post('otpValue');
            $otpRegistrationNo = $this->CI->session->userdata('otpsmsid');
            // pre_obj($this->session);
            // Verify OTP
            //$response = $this->verifySMSOTP($otpValue, $otpRegistrationNo);
           // $response = json_decode($response, true);
            if (is_localhost_request()) {
                $response['status'] = ucwords("Otp Verified");
                
            } else {
                $response = $this->verifySMSOTP($otpValue, $otpRegistrationNo);
                $response = json_decode($response, true);
            }
            //pre_obj($response);
            if (strtolower($response['status']) == 'success' || $response['status'] == "Otp Verified") {
                $response['status'] = ucwords("Otp Verified");
                $response['code'] = 100;
                $logindata = [                
                    'loginStatus' => true  

                ];
                 $this->CI->session->set_userdata($logindata);
            } else {
                $response['status'] = $response['error_message'];
                $response['code'] = -1;
            }
            //pre_obj($this->CI->session);
            $response = array($response);
            return $response;
        } else {

            $errorArray = $this->CI->form_validation->error_array();
            $response = array();
            array_push($response, array('status' => $errorArray[key($errorArray)], 'code' => -1));
            return $response;
        }
    }

    

}
