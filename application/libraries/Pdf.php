<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * CodeIgniter PDF Library
 *
 * @package         CodeIgniter
 * @subpackage      Libraries
 * @category        Libraries
 * @author          Muhanz
 * @license         MIT License
 * @link            https://github.com/hanzzame/ci3-pdf-generator-library
 *
 */

    require_once(dirname(__FILE__) . '/dompdf/autoload.inc.php');
    use Dompdf\Dompdf;
    //setPaper('A4', 'landscape');
    class Pdf
    {
        public function create($html,$filename, $stream=TRUE, $paper = 'A4', $orientation = "landscape")
        {
            
            
            $dompdf = new Dompdf($options);
            
            //$dompdf = new Dompdf($options);
            $dompdf->set_option('enable_html5_parser', TRUE);
            $dompdf->set_paper($paper, $orientation);
            $dompdf->loadHtml($html);
            $dompdf->render();
            if ($stream) 
            {
                $dompdf->stream($filename.".pdf", array("Attachment" => 0));
            } else {
                return $dompdf->output();
            }
        }
    }




