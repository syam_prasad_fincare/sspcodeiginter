<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Customencryption {

    public $CI;    
    public function __construct()
    {
        $this->CI =& get_instance();
    }

    
    public function decryptPOSTRequest($arr = []) {
       $_POST = $this->cryptoJsAesDecrypt($this->CI->config->item("aesencryption_key"),$arr);
    }
    
    /**
    * Decrypt data from a CryptoJS json encoding string
    *
    * @param mixed $passphrase
    * @param mixed $jsonString
    * @return mixed
    */
    // function cryptoJsAesDecrypt($passphrase, $jsonString){
    //     $jsondata = json_decode($jsonString, true);
    //     $salt = hex2bin($jsondata["s"]);
    //     $ct = base64_decode($jsondata["ct"]);
    //     $iv  = hex2bin($jsondata["iv"]);
    //     $concatedPassphrase = $passphrase.$salt;
    //     $md5 = array();
    //     $md5[0] = md5($concatedPassphrase, true);
    //     $result = $md5[0];
    //     for ($i = 1; $i < 3; $i++) {
    //         $md5[$i] = md5($md5[$i - 1].$concatedPassphrase, true);
    //         $result .= $md5[$i];
    //     }
    //     $key = substr($result, 0, 32);
    //     $data = openssl_decrypt($ct, 'aes-256-cbc', $key, true, $iv);
    //     return json_decode($data, true);
    // }
    function cryptoJsAesDecrypt($passphrase, $jsonString)
    {
       
        $cypher = 'aes-256-cbc';  
        $ivSize  = openssl_cipher_iv_length($cypher);//var_dump($ivSize);
       
        $hasher = "sha512"; // CryptoJS uses SHA1 by default
        $iterations = 500;
        $outsize = 256;
        $jsonst=json_decode(base64_decode($jsonString),true);
        $sa=$jsonst['salt'];//var_dump(base64_decode($sa));
        $iv=hex2bin($jsonst['iv']);//var_dump($iv);
        $salt2=$jsonst['salt2'];
        $encrypted_data =base64_decode($jsonst["ciphertext"]);
        $hashKey = hash_pbkdf2($hasher, $passphrase, base64_decode($sa), $iterations, $outsize/8, true);

        $data2 =openssl_decrypt($encrypted_data, 'AES-256-CBC', $hashKey,OPENSSL_RAW_DATA|OPENSSL_ZERO_PADDING, $iv);
         //var_dump(($data2)); 
        $padding = ord($data2[strlen($data2) - 1]); 
        $toutput= substr($data2, 0, -$padding); 
      
            
         
        return json_decode($toutput, true);
     }

    /**
    * Encrypt value to a cryptojs compatiable json encoding string
    *
    * @param mixed $passphrase
    * @param mixed $value
    * @return string
    */
    // function cryptoJsAesEncrypt($passphrase, $value){
    //     $salt = openssl_random_pseudo_bytes(8);
    //     $salted = '';
    //     $dx = '';
    //     while (strlen($salted) < 48) {
    //         $dx = md5($dx.$passphrase.$salt, true);
    //         $salted .= $dx;
    //     }
    //     $key = substr($salted, 0, 32);
    //     $iv  = substr($salted, 32,16);
    //     $encrypted_data = openssl_encrypt(json_encode($value), 'aes-256-cbc', $key, true, $iv);
    //     $data = array("ct" => base64_encode($encrypted_data), "iv" => bin2hex($iv), "s" => bin2hex($salt));
    //     return json_encode($data);
    // }
    function CryptoJSAesEncrypt($passphrase, $data)
    {

        $salt = openssl_random_pseudo_bytes(256);
        $iv = openssl_random_pseudo_bytes(16);
        //on PHP7 can use random_bytes() istead openssl_random_pseudo_bytes()
      
        $iterations = 999; 
        $key = hash_pbkdf2("sha512", $passphrase, $salt, $iterations, 64);

        $encrypted_data = openssl_encrypt($data, 'aes-256-cbc', hex2bin($key), OPENSSL_RAW_DATA, $iv);

        $data = array("ciphertext" => base64_encode($encrypted_data), "iv" => bin2hex($iv), "salt" => bin2hex($salt));
        return json_encode($data);
    }


}
