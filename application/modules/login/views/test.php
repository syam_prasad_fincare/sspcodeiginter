<!DOCTYPE html>
<html>

<head>
  <link data-require="bootstrap@4.0.0-alpha.6" data-semver="4.0.0-alpha.6" rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" />
  <script data-require="bootstrap@4.0.0-alpha.6" data-semver="4.0.0-alpha.6" src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js"></script>
  <script data-require="tether@1.4.0" data-semver="1.4.0" src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
  <!-- <link rel="stylesheet" href="style.css" /> -->
<style type="text/css">
    /* Style */

.bigone {
  background: lightgrey;
}

.header {
  background: green;
}

.mybar {
  background: pink;
}

.footer {
  background: cyan;
}

/* Logic */

.bigone {
  display: flex;
  min-height: 100vh;
  flex-direction: column;
}

.main {
    flex: 1 1 0;
    display: flex;
    flex-direction: column;
    overflow-y: scroll;
}
</style>
</head>

<body>

  <!-- HERE I HAVE one div automatically generated with randomly ID and class -->

  <div class="bigone">

    <div class="container-fluid">

      <div class="header">
        My big header
      </div>

    </div>

    <div class="mybar">
      Nav bar
    </div>

    <div class="main">
      <div class="container-fluid">
        <div class="row">

          <div class="col-6">
            <div class="card">
              <div class="card-header">
                Card Header
              </div>
              <div class="list-group list-group-flush">
                <a href="#" class="list-group-item list-group-item-action"><b>FIRST LINK</b></a>
                <a href="#" class="list-group-item list-group-item-action">Dapibus ac facilisis in</a>
                <a href="#" class="list-group-item list-group-item-action">Morbi leo risus</a>
                <a href="#" class="list-group-item list-group-item-action">Porta ac consectetur ac</a>
                <a href="#" class="list-group-item list-group-item-action disabled">Vestibulum at eros</a>
                <a href="#" class="list-group-item list-group-item-action">Dapibus ac facilisis in</a>
                <a href="#" class="list-group-item list-group-item-action">Morbi leo risus</a>
                <a href="#" class="list-group-item list-group-item-action">Porta ac consectetur ac</a>
                <a href="#" class="list-group-item list-group-item-action disabled">Vestibulum at eros</a>
                <a href="#" class="list-group-item list-group-item-action">Dapibus ac facilisis in</a>
                <a href="#" class="list-group-item list-group-item-action">Morbi leo risus</a>
                <a href="#" class="list-group-item list-group-item-action">Porta ac consectetur ac</a>
                <a href="#" class="list-group-item list-group-item-action disabled">Vestibulum at eros</a>
                <a href="#" class="list-group-item list-group-item-action">Dapibus ac facilisis in</a>
                <a href="#" class="list-group-item list-group-item-action">Morbi leo risus</a>
                <a href="#" class="list-group-item list-group-item-action">Porta ac consectetur ac</a>
                <a href="#" class="list-group-item list-group-item-action disabled">Vestibulum at eros</a>
                <a href="#" class="list-group-item list-group-item-action">Dapibus ac facilisis in</a>
                <a href="#" class="list-group-item list-group-item-action">Morbi leo risus</a>
                <a href="#" class="list-group-item list-group-item-action">Porta ac consectetur ac</a>
                <a href="#" class="list-group-item list-group-item-action disabled">Vestibulum at eros</a>
                <a href="#" class="list-group-item list-group-item-action">Dapibus ac facilisis in</a>
                <a href="#" class="list-group-item list-group-item-action">Morbi leo risus</a>
                <a href="#" class="list-group-item list-group-item-action">Porta ac consectetur ac</a>
                <a href="#" class="list-group-item list-group-item-action disabled">Vestibulum at eros</a>
                <a href="#" class="list-group-item list-group-item-action">Dapibus ac facilisis in</a>
                <a href="#" class="list-group-item list-group-item-action">Morbi leo risus</a>
                <a href="#" class="list-group-item list-group-item-action">Porta ac consectetur ac</a>
                <a href="#" class="list-group-item list-group-item-action disabled">Vestibulum at eros</a>
                <a href="#" class="list-group-item list-group-item-action">Dapibus ac facilisis in</a>
                <a href="#" class="list-group-item list-group-item-action">Morbi leo risus</a>
                <a href="#" class="list-group-item list-group-item-action">Porta ac consectetur ac</a>
                <a href="#" class="list-group-item list-group-item-action disabled">Vestibulum at eros</a>
                <a href="#" class="list-group-item list-group-item-action">Dapibus ac facilisis in</a>
                <a href="#" class="list-group-item list-group-item-action">Morbi leo risus</a>
                <a href="#" class="list-group-item list-group-item-action">Porta ac consectetur ac</a>
                <a href="#" class="list-group-item list-group-item-action disabled">Vestibulum at eros</a>
                <a href="#" class="list-group-item list-group-item-action">Dapibus ac facilisis in</a>
                <a href="#" class="list-group-item list-group-item-action">Morbi leo risus</a>
                <a href="#" class="list-group-item list-group-item-action">Porta ac consectetur ac</a>
                <a href="#" class="list-group-item list-group-item-action disabled">Vestibulum at eros</a>
                <a href="#" class="list-group-item list-group-item-action">Dapibus ac facilisis in</a>
                <a href="#" class="list-group-item list-group-item-action">Morbi leo risus</a>
                <a href="#" class="list-group-item list-group-item-action">Porta ac consectetur ac</a>
                <a href="#" class="list-group-item list-group-item-action disabled">Vestibulum at eros</a>
                <a href="#" class="list-group-item list-group-item-action">Dapibus ac facilisis in</a>
                <a href="#" class="list-group-item list-group-item-action">Morbi leo risus</a>
                <a href="#" class="list-group-item list-group-item-action">Porta ac consectetur ac</a>
                <a href="#" class="list-group-item list-group-item-action disabled">Vestibulum at eros</a>
                <a href="#" class="list-group-item list-group-item-action">Dapibus ac facilisis in</a>
                <a href="#" class="list-group-item list-group-item-action">Morbi leo risus</a>
                <a href="#" class="list-group-item list-group-item-action">Porta ac consectetur ac</a>
                <a href="#" class="list-group-item list-group-item-action disabled">Vestibulum at eros</a>
                <a href="#" class="list-group-item list-group-item-action disabled"><b>LAST LINK</b></a>
              </div>
              <div class="card-footer">
                Card Footer
              </div>
            </div>
          </div>

          <div class="col-6">
            <h1>FIRST LINE</h1> So many words, so many words. So many words, so many words. So many words, so many words.
            <br> So many words, so many words. So many words, so many words. So many words, so many words.
            <br> So many words, so many words. So many words, so many words. So many words, so many words.
            <br> So many words, so many words. So many words, so many words. So many words, so many words.
            <br> So many words, so many words. So many words, so many words. So many words, so many words.
            <br> So many words, so many words. So many words, so many words. So many words, so many words.
            <br> So many words, so many words. So many words, so many words. So many words, so many words.
            <br> So many words, so many words. So many words, so many words. So many words, so many words.
            <br> So many words, so many words. So many words, so many words. So many words, so many words.
            <br> So many words, so many words. So many words, so many words. So many words, so many words.
            <br> So many words, so many words. So many words, so many words. So many words, so many words.
            <br> So many words, so many words. So many words, so many words. So many words, so many words.
            <br> So many words, so many words. So many words, so many words. So many words, so many words.
            <br> So many words, so many words. So many words, so many words. So many words, so many words.
            <br> So many words, so many words. So many words, so many words. So many words, so many words.
            <br> So many words, so many words. So many words, so many words. So many words, so many words.
            <br> So many words, so many words. So many words, so many words. So many words, so many words.
            <br> So many words, so many words. So many words, so many words. So many words, so many words.
            <br> So many words, so many words. So many words, so many words. So many words, so many words.
            <br> So many words, so many words. So many words, so many words. So many words, so many words.
            <br> So many words, so many words. So many words, so many words. So many words, so many words.
            <br> So many words, so many words. So many words, so many words. So many words, so many words.
            <br> So many words, so many words. So many words, so many words. So many words, so many words.
            <br> So many words, so many words. So many words, so many words. So many words, so many words.
            <br> So many words, so many words. So many words, so many words. So many words, so many words.
            <br> So many words, so many words. So many words, so many words. So many words, so many words.
            <br> So many words, so many words. So many words, so many words. So many words, so many words.
            <br> So many words, so many words. So many words, so many words. So many words, so many words.
            <br> So many words, so many words. So many words, so many words. So many words, so many words.
            <br> So many words, so many words. So many words, so many words. So many words, so many words.
            <br> So many words, so many words. So many words, so many words. So many words, so many words.
            <br> So many words, so many words. So many words, so many words. So many words, so many words.
            <br> So many words, so many words. So many words, so many words. So many words, so many words.
            <br> So many words, so many words. So many words, so many words. So many words, so many words.
            <br> So many words, so many words. So many words, so many words. So many words, so many words.
            <br> So many words, so many words. So many words, so many words. So many words, so many words.
            <br> So many words, so many words. So many words, so many words. So many words, so many words.
            <br> So many words, so many words. So many words, so many words. So many words, so many words.
            <br> So many words, so many words. So many words, so many words. So many words, so many words.
            <br> So many words, so many words. So many words, so many words. So many words, so many words.
            <br> So many words, so many words. So many words, so many words. So many words, so many words.
            <br> So many words, so many words. So many words, so many words. So many words, so many words.
            <br> So many words, so many words. So many words, so many words. So many words, so many words.
            <br> So many words, so many words. So many words, so many words. So many words, so many words.
            <br> So many words, so many words. So many words, so many words. So many words, so many words.
            <br> So many words, so many words. So many words, so many words. So many words, so many words.
            <br> So many words, so many words. So many words, so many words. So many words, so many words.
            <br> So many words, so many words. So many words, so many words. So many words, so many words.
            <br> So many words, so many words. So many words, so many words. So many words, so many words.
            <br> So many words, so many words. So many words, so many words. So many words, so many words.
            <br> So many words, so many words. So many words, so many words. So many words, so many words.
            <br> So many words, so many words. So many words, so many words. So many words, so many words.
            <br> So many words, so many words. So many words, so many words. So many words, so many words.
            <br> So many words, so many words. So many words, so many words. So many words, so many words.
            <br> So many words, so many words. So many words, so many words. So many words, so many words.
            <br> So many words, so many words. So many words, so many words. So many words, so many words.
            <br> So many words, so many words. So many words, so many words. So many words, so many words.
            <br> So many words, so many words. So many words, so many words. So many words, so many words.
            <br> So many words, so many words. So many words, so many words. So many words, so many words.
            <br> So many words, so many words. So many words, so many words. So many words, so many words.
            <br> So many words, so many words. So many words, so many words. So many words, so many words.
            <br> So many words, so many words. So many words, so many words. So many words, so many words.
            <br> So many words, so many words. So many words, so many words. So many words, so many words.
            <br> So many words, so many words. So many words, so many words. So many words, so many words.
            <br> So many words, so many words. So many words, so many words. So many words, so many words.
            <br> So many words, so many words. So many words, so many words. So many words, so many words.
            <br> So many words, so many words. So many words, so many words. So many words, so many words.
            <br> So many words, so many words. So many words, so many words. So many words, so many words.
            <br> So many words, so many words. So many words, so many words. So many words, so many words.
            <br> So many words, so many words. So many words, so many words. So many words, so many words.
            <br> So many words, so many words. So many words, so many words. So many words, so many words.
            <br> So many words, so many words. So many words, so many words. So many words, so many words.
            <br> So many words, so many words. So many words, so many words. So many words, so many words.
            <br> So many words, so many words. So many words, so many words. So many words, so many words.
            <br> So many words, so many words. So many words, so many words. So many words, so many words.
            <br> So many words, so many words. So many words, so many words. So many words, so many words.
            <br> So many words, so many words. So many words, so many words. So many words, so many words.
            <br> So many words, so many words. So many words, so many words. So many words, so many words.
            <br> So many words, so many words. So many words, so many words. So many words, so many words.
            <br> So many words, so many words. So many words, so many words. So many words, so many words.
            <br> So many words, so many words. So many words, so many words. So many words, so many words.
            <br>
            <h1>LAST LINE</h1>
          </div>

        </div>
      </div>
    </div>

    <div class="footer">
      Footer
    </div>

  </div>

  <!-- HERE THAT DIV CLOSED -->

</body>

</html>