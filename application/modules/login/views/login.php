	<div class="container" >
		<div class="row">
			<div class="col-md-8 col-sm-12">				
				<div class="banner-pic"><img id ="img" src ="<?=$this->config->item('images_path')?>SSP_login_banner.png" alt= "ssp_login_banner" ></div>				
			</div>
			
			<div class="col-md-4 col-sm-12">
				<div class= "login-page">
				  <form id="loginForm">
				  	<input type="hidden" id="usertype" value="login" />
				  	<div class="row">
						<img id="image" src ="<?=$this->config->item('images_path')?>fin_image.png" alt = "Fincare logo" > 
					</div>
					<div class="row form-group">
					 <p class="msg" style="margin: 1% 15% !important;width: 70%; text-align: center;"></p>
						<input id = "userid" type="text" name="userid" autocomplete="off"  placeholder= "User name" required /><br>
					</div>
					<div class="row form-group">
						<input id = "userpw" type="password" name="userpw" placeholder="Password" 
					required />  
					</div>
					<a href="https://ib.fincarebank.com/RIB/#/" target="__blank" > <h6> Forgot Password? </h6></a>
					<div class="row form-group">
						<button id="button1" type="submit" ><b>LOGIN</b> </button>
					</div>
				</form>
				
				<br style="clear:both" />

				
                <form id="otpForm" class="otp_module otp_form d-none">
                		<p class="msg" style="margin: 1% 15% !important;width: 70%; text-align: center;"></p>
						<div class="ot">
							<input type="password" id="otpValue" name="otpValue" class="ot" placeholder="Please Enter OTP" value="" required /><br>
						</div>
						
						<!-- <div id="o"></div> -->
						<!-- <p id="count"></p> -->
						<span id="resendMSG" style="margin: 0% 9% !important;width: 70%; text-align: center;" class="d-none">You can resend otp after <b id="resendTime"></b></span>
						<a href="javascript:void(0)" id="resendBtn" class="d-none resend_otp" >Resend OTP</a>
						<!-- <div class="ref"><a href="#" id = "anc" onclick="fun()"> Resend OTP </a></div>			  -->
						<button id="button2" type="submit"><b>SUBMIT</b></button><br>
					</form> 
					
			</div>
			
		</div>
	</div>
</div>