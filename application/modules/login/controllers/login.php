<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends MY_Controller {

	public function __construct()
    {
            parent::__construct();
           
            
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                if(isset($_POST['data'])){
                    $this->load->library("Customencryption");
                    $this->customencryption->decryptPOSTRequest($_POST['data']);
                }
           }
           
            
            //Your own constructor code
    }
	public function index()
	{ 
		$data['header'] = 'layout/header';
		$data['page'] = 'login';
		$data['footer'] = 'layout/footer';
		$data['login'] = TRUE;
    $data['title'] = "Login";
		$this->load->view('template',$data);
	}
	public function validateCredentials()
	{
		//pre_obj($_POST);
		$response = $this->commonlibrary->validateLogin($this->input->post());
       //pre_obj($response);
       if(empty($response[0]['code'])){
            
            $logindata = [
                'role' => 'Super_user',
                'userName' => $response[0]['result']['name'],
                'lastLogin' => $response[0]['result']['last_login_dt'],
                'loginStatus' => true    
            ];
            $this->session->set_userdata($logindata);
       }
       echo json_encode($response);
	}
     public function validateOTP(){
        
        try{

            $response=$this->commonlibrary->loginStepTwo();
            // pre_obj($this->session);
                 if($response[0]['code'] == 100)
                  {

                     
                     

                  }

                  die(json_encode($response));

              }
              catch(Exception $e){

                  $responseArray=array();
                  array_push($responseArray,array('status' => $e->getMessage() , 'code' => -1));
                  die(json_encode($responseArray));

              }

    }
    public function resendLoginOTP(){

        try{


            $responseArray=array();
            
            $otpDataInput=array(   
                'mobileno'=> $this->session->userdata('mobileNo')
            );
            if (is_localhost_request()) {
                $otpResponse['status'] = "Success";
                $otpResponse['ReferenceNumber'] = "817c02f9-85a6-423d-ae9d-927ead4f7273";
            } else {
                $otpResponse = $this->sendSMSOTP($otpDataInput);
            }
           // $otpResponse=$this->commonlibrary->sendSMSOTP($otpDataInput);

            if(strtolower($otpResponse['status'])=='success'){
                $otpResponse['status']=resendOTPText;
                $otpResponse['code']=0;
            }
            else{
                $otpResponse['code']=-1;
            }

            array_push($responseArray,$otpResponse);
            die(json_encode($responseArray));

            
        }
        catch(Exception $e){

            $responseArray=array();
            array_push($responseArray,array('status' => $e->getMessage() , 'code' => -1));
            die(json_encode($responseArray));

        }

    }


}
