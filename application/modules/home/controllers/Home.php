<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MY_Controller {

	public function __construct()
    {
            parent::__construct();
           
           	if (!isset($this->session->loginStatus) ) {
				
					redirect('login');
			}
            
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                if(isset($_POST['data'])){
                    $this->load->library("Customencryption");
                    $this->customencryption->decryptPOSTRequest($_POST['data']);
                }
           }
           
            
            //Your own constructor code
    }
	public function index()
	{
		//pre_obj($this->session);
        $data['title'] = "Home";
		$data['header'] = 'layout/header';
		$data['page'] = 'home';
		$data['footer'] = 'layout/footer';
		$data['home'] = FALSE;
		$this->load->view('template',$data);
		//$this->load->view('welcome_message');
	}
	public function logout() {


        
        // Session Destroy
        $this->session->sess_destroy();
        // delete cookies 
        delete_cookie("bankfincare");
        delete_cookie("ci_session");
        redirect('login');
        
      }
}
