<style type="text/css">
	.main {
    flex: 1 1 0;
    display: flex;
    flex-direction: column;
    overflow-y: scroll;
    height: 450px;
}
</style>
<div class="main">
	

	<div id="authentication">
	  <br>
	  <div class="container">
	    <h4>Designing your card is easy</h4>
	    <br>
	    <h6 style="line-height: unset;font-weight: 400;">The days of boring debit cards are over. Now you can share your pride and joy everytime you use your card</h6>
	   <br><br>
	    <div class="row" style="text-align: center;">
	     <div class="col-6 col-lg-6 col-md-offset-3">
	   <h6>Edge-to-Edge Photo</h6>
	   <br>
	   <img style="height:110px;transform: skewX(2deg);" src="assets/images/image_edge_to_edge.png" alt=""><br> <br>
	   <a style="color:#A8060A;font-weight:500;" id="photo1" onclick="show('TermsandCond','e2e')">Get started here <img src="assets/images/ic_menu (2).svg" alt=""></a>
	     </div>
	     <div class="col-6 col-lg-6 vl">
	       <h6>1X1" Photo</h6>
	       <br>
	       <img style="height:110px;transform: skewX(-2deg);" src="assets/images/image_1_x_1_photo.png" alt=""><br> <br>
	       <a style="color:#A8060A;font-weight:500;" id="photo2" onclick="show('TermsandCond','o2o')">Get started here <img src="assets/images/ic_menu (2).svg" alt=""></a>
	     </div>
	     <input type="hidden" id="photovalue">
	   </div>
	  </div>  
	</div>




	<div id="cardDetails" style="display:none;">
	    <br>
	    <div class="container">
	        <h4>Thank you</h4>
	        <h5 style="color:orange">Successfully submitted your design</h5>
	        <h6 style="color:gray;margin-top:15px;">Reference ID:</h6>
	        <h6 style="margin-top:-3px;"><span id="referno"></span></h6>
	    <br> 

	        <h6 style="line-height: unset;font-weight: 400;">Fincare bank will review your design and send you a confirmation email within 2 business day.</h6>
	    <br>
	        <h6 style="line-height: unset;font-weight: 400;">Uploaded images with third party copyrighted content will not be approved.</h6>
	    <br>
	        <h6 style="line-height: unset;font-weight: 400;">Once your design is approved, your new card will then arrrive within 5 to 7 business days</h6>

	    <br><br><br>
	        <input type="button" style="height:50px;" class="btn btn-next" onclick="show('authentication')" value="Go To Home">
	    </div>
	</div>

	<div id="TermsandCond" style="display:none;">


	    <div class="container">
	        <br>
	        <h4>Requirements</h4>
	        <br>
	        <p style="color:orange;font-size:12px;font-weight: 500;">UPLOAD GUIDELINES</p>

	        <div class="row">
	          <div class="col-1"><img src="assets/images/pmenu.svg" alt=""></div>
	          <div class="col-11"><p>Image files can be in either .JPEG, .GIF, .PNG, .TIFF or .BMP formats</p></div>
	        </div>
	        <div class="row">
	          <div class="col-1"><img src="assets/images/pmenu.svg" alt=""></div>
	          <div class="col-11"><p>The maximum file size is 5 megabytes (MB)</p></div>
	        </div>
	        <div class="row">
	          <div class="col-1"><img src="assets/images/pmenu.svg" alt=""></div>
	          <div class="col-11"><p>The minimum file size is 300 kilobytes (KB)</p></div>
	        </div>
	        <div class="row">
	          <div class="col-1"><img src="assets/images/pmenu.svg" alt=""></div>
	          <div class="col-11"><p>For <b>Edge-to-Edge</b> Photo must be atleast 640 X 480 pixels</p></div>
	        </div>
	        <div class="row">
	          <div class="col-1"><img src="assets/images/pmenu.svg" alt=""></div>
	          <div class="col-11"><p>For <b>1X1"</b> Photo must be atleast 400 X 400 pixels</p></div>
	        </div>
	        <div class="row">
	          <div class="col-1"><img src="assets/images/pmenu.svg" alt=""></div>
	          <div class="col-11"><p>Small images will reproduce poorly when printed</p></div>
	        </div>
	        <div class="row">
	          <div class="col-1"><img src="assets/images/pmenu.svg" alt=""></div>
	          <div class="col-11"><p>If you increase the scale of your image too much, it may also print poorly</p></div>
	        </div>
	        <div class="row">
	          <div class="col-1"><img src="assets/images/pmenu.svg" alt=""></div>
	          <div class="col-11"><p>The bigger the image you choose the longer it will take to upload</p></div>
	        </div>
	        <div class="row">
	          <div class="col-1"><img src="assets/images/pmenu.svg" alt=""></div>
	          <div class="col-11"><p>The time your image takes to upload depends on your own internet connection speed</p></div>
	        </div>
	        <div class="row">
	          <div class="col-1"><img src="assets/images/pmenu.svg" alt=""></div>
	          <div class="col-11"><p>If your image is slow to upload please talk to your own internet access provider for help</p></div>
	        </div>


	        <p style="color:orange;font-size:12px;font-weight: 500;">IMAGE AND TEXT GUIDELINES</p>

	        <p><b>Fincare SFB</b> reserves the right to determine, in its sole discretion, whether a submitted image will be accepted. All the images downloaded from the Internet. <b>Fincare SFB</b> will not accept images that contain any of the following</p>

	        <div class="row">
	          <div class="col-1"><img src="assets/images/pmenu.svg" alt=""></div>
	          <div class="col-11"><p>Any verbage other than the business name in the text lines on the card.</p></div>
	        </div>
	        <div class="row">
	          <div class="col-1"><img src="assets/images/pmenu.svg" alt=""></div>
	          <div class="col-11"><p>Company slogans of any type, whether contained in the image or in the text.</p></div>
	        </div>
	        <div class="row">
	          <div class="col-1"><img src="assets/images/pmenu.svg" alt=""></div>
	          <div class="col-11"><p>Trademarks, copyrighted materials, company names, logos, slogans, brands of any third party and any material that could infringe on the copyright or trademark rights of another party.</p></div>
	        </div>
	        <div class="row">
	          <div class="col-1"><img src="assets/images/pmenu.svg" alt=""></div>
	          <div class="col-11"><p>Phone numbers, addresses. account numbers, personal identification numbers or URL addresses.</p></div>
	        </div>
	        <div class="row">
	          <div class="col-1"><img src="assets/images/pmenu.svg" alt=""></div>
	          <div class="col-11"><p>Celebrities, actors musicians, sports figures, politicians, cartoon characters or public figures of any kind.</p></div>
	        </div>
	        <div class="row">
	          <div class="col-1"><img src="assets/images/pmenu.svg" alt=""></div>
	          <div class="col-11"><p>Materials / content demonstrating the views and / or opinions of political groups, religious groups or anti-social or society unacceptable groups.</p></div>
	        </div>
	        <div class="row">
	          <div class="col-1"><img src="assets/images/pmenu.svg" alt=""></div>
	          <div class="col-11"><p>People conducting or involving themselves in criminal or illegal activities or otherwise inappropriate behaviour</p></div>
	        </div>
	        <div class="row">
	          <div class="col-1"><img src="assets/images/pmenu.svg" alt=""></div>
	          <div class="col-11"><p>Culturally or socially sensitive material.</p></div>
	        </div>
	        <div class="row">
	          <div class="col-1"><img src="assets/images/pmenu.svg" alt=""></div>
	          <div class="col-11"><p>Provactive or derogatory material or images of a sexual nature or containing any sexual content.</p></div>
	        </div>
	        <div class="row">
	          <div class="col-1"><img src="assets/images/pmenu.svg" alt=""></div>
	          <div class="col-11"><p>Nudity or semi-nudity.</p></div>
	        </div>
	        <div class="row">
	          <div class="col-1"><img src="assets/images/pmenu.svg" alt=""></div>
	          <div class="col-11"><p>Violent or other offensive material including material that could be perceived as violent</p></div>
	        </div>
	        <div class="row">
	          <div class="col-1"><img src="assets/images/pmenu.svg" alt=""></div>
	          <div class="col-11"><p>Obscene or indecent material including profanity or obscenities.</p></div>
	        </div>
	        <div class="row">
	          <div class="col-1"><img src="assets/images/pmenu.svg" alt=""></div>
	          <div class="col-11"><p>Racial, prejudicial or discriminatory subject matter</p></div>
	        </div>

	        <br>
	        <p><b>Fincare SFB</b> reserves the right to determine in its sole discretion whether a submitted image will be accepted. All images submitted must have the consent of the owner including those images downloaded from the internet. <b>Fincare SFB</b> will not accept images that contain any of the items listed in the image guidelines. Images that do not meet the image guidelines will be rejected and you will be notified by email. You may then submit another image</p>
	        <br>
	        <input style="height:50px;" type="button" class="btn btn-next" onclick="show('resetPin')" value="NEXT">

	        <br><br>
	  </div>
	</div>


	<div id="resetPin" style="display:none;">
	  <br>

	    <div class="container">
	      <h4>Debit card Validations</h4>
	      <p>Lets get started. The following information is needed for you to validate your Fincare bank account with us. Please enter your information in the fields below:</p>

	      <br>
	       <div class="alert alert-danger alert-dismissable" id="myAlert2" style="display: none;">
	            <!-- <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> -->
	           Incorrect debit card number
	        </div>

	      <div class="row">
	          <div class="col-2">
	              <label>Select Account:</label>
	          </div>
	           <div class="col-6">
	              <select class="form-control" id="account" name="account">
	                  <option value=""> Select Account Number</option>
	                 
	              </select>
	          </div>
	      </div>
	      <br>
	      <br>

	      <p>Enter last four digits of your Debit Card</p>
	      <input class="otp inputs" type="text" id="inputs1" maxLength="1" size="1" min="0" max="9" pattern="[0-9]{1}" />
	      <input class="otp inputs" type="text" id="inputs2" maxLength="1" size="1" min="0" max="9" pattern="[0-9]{1}" />
	      <input class="otp inputs" type="text" id="inputs3" maxLength="1" size="1" min="0" max="9" pattern="[0-9]{1}" />
	      <input class="otp inputs" type="text" id="inputs4"  maxLength="1" size="1" min="0" max="9" pattern="[0-9]{1}" />
	   
	     <br><br>
	      <div class="form-check">
	        <label id="consentMessage" class="form-check-label">
	          <input type="checkbox" id="confirmCheckBox" onclick="toggleCheckBox()" class="form-check-input" value="" style="width: 17px;
	    height: 20px;">I understand that the personalized debit card costs Rs.499 + applicable taxes and the fee will be deducted from my account linked to my debit card. I also confirm that I have read, understood and accepted the general Terms and Conditions.
	        </label>
	      </div>
	      <br><br>
	      <input type="button" id="cardValidate" style="height:50px" class="btn btn-next"  value="NEXT">
	    </div>



	</div>

	<div id="debitUpload" style="display:none;">
	  <div class="container">
	    <br>
	    <h4>Upload your image</h4>
	    <br>

	    <div class="contain" >
	        
	      <img src="assets/images/personalized_photo.png" alt="" id="fullPreview">

	        <div class="top-right">
	        <img src="assets/images/log.svg" alt="">
	      </div>
	      <div class="bottom-right">
	        <img src="assets/images/ic_rupay (1).svg" alt="">
	      </div>
	      <div class="bottom-right1">
	        <img src="assets/images/ic_debit (1).svg" alt="">
	      </div>
	      <div class="chip">
	        <img src="assets/images/ic_chip (3).svg" alt="">
	      </div>
	      <div class="centered">
	        <span id="cardno">
	            6505   8767   5436   7896
	        </span>
	      </div>
	      <div class="valid">
	        <img src="assets/images/ic_valid_thru.svg" alt=""> 
	        <span id="cardexpriy">
	            07/25
	        </span>
	      </div>
	      <div class="bottom-name">
	      SAMEER  CHAKRABORTHY
	      </div>
	    </div>
	    
	    <br><br>
	    <input type="file" id="imgupload" style="display:none"/>
	    <div style="text-align:center;">
	      <!-- <button class="btn" onclick="show('uploadedDebit')" style="font-size: 18px;margin-right:8px;font-weight:500;height:50px;border:1px solid #A8060A;color:#A8060A;width:40%;">Take a photo</button> -->
	      <button class="btn" onclick="video_cat()" style="font-size: 18px;margin-right:8px;font-weight:500;height:50px;border:1px solid #A8060A;color:#A8060A;width:40%;">Take a photo</button>

	      <button class="btn" onClick="onClickHandler(event)" style="font-size: 18px;margin-left:8px;font-weight:500;height:50px;border:1px solid #A8060A;color:#A8060A;width:40%;">From gallery</button>
	    </div>
	    <br>
	    <br>
	    <div class="row">
	        <input type="button" style="height:50px;" id ="nextUpload" class="btn btn-next" onclick="show('uploadedDebit')" value="NEXT">
	     <!--   <button class="btn col-md-offset-3" onclick="show('uploadedDebit')" style="font-size: 18px;margin-right:8px;font-weight:500;height:50px;border:1px solid #A8060A;color:#A8060A;width:20%;">Next</button> -->
	        
	    </div>
	  </div>

	</div>
	<div id="debitUpload1" style="display:none;">
	  <div class="container">
	    <br>
	    <h4>Upload your image</h4>
	    <br>

	    <div class="contain">
	      <img src="assets/images/dynamic_101.svg"  alt="" >
	      <div class="top-right12">
	      <img src="assets/images/101Crop.png" style="height:70px;width:75px;" alt="" id="passout">
	      </div>
	      <div class="centered">
	        <span id="cardno1x1">
	              6505   8767   5436   7896
	        </span>
	      
	      </div>
	      <div class="valid">
	        <span id="cardexpriy1x1">
	             07/25
	        </span>
	       
	      </div>
	      <div class="bottom-name">
	        SAMEER
	      </div>
	    </div>
	 
	    <br><br>
	    <div style="text-align:center;">
	      <button class="btn" onclick="show('uploadedDebit1')" style="font-size: 18px;margin-right:8px;font-weight:500;height:50px;border:1px solid #A8060A;color:#A8060A;width:40%;">Take a photo</button>
	      <button class="btn" onClick="onClickHandler(event)" style="font-size: 18px;margin-left:8px;font-weight:500;height:50px;border:1px solid #A8060A;color:#A8060A;width:40%;">From gallery</button>
	    </div>
	     <br>
	    <br>
	    <div class="row">
	        <input type="button" style="height:50px;" id ="nextUpload" class="btn btn-next" onclick="show('uploadedDebit')" value="NEXT">
	     <!--   <button class="btn col-md-offset-3" onclick="show('uploadedDebit')" style="font-size: 18px;margin-right:8px;font-weight:500;height:50px;border:1px solid #A8060A;color:#A8060A;width:20%;">Next</button> -->
	        
	    </div>
	  </div>

	</div>

	<div id="uploadedDebit" style="display:none;">

	    
	  <div class="container">
	    <br>
	    <div class="alert alert-danger alert-dismissable" id="myAlert3" style="display: none;">
	        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	      <span id="error-dms"></span>
	    </div>
	    <br>
	    <h4>Upload your image</h4>
	    <br>
	    <div class="contain">
	      <img src="assets/images/personalized_photo.png" alt="" id="fullConfirmPreview" >
	      <!--  -->
	      <div class="top-right">
	        <img src="assets/images/log.svg" alt="">
	      </div>
	      <div class="bottom-right">
	        <img src="assets/images/ic_rupay (1).svg" alt="">
	      </div>
	      <div class="bottom-right1">
	        <img src="assets/images/ic_debit (1).svg" alt="">
	      </div>
	      <div class="chip">
	        <img src="assets/images/ic_chip (3).svg" alt="">
	      </div>
	      <div class="centered">
	        <span id="cardno1">
	            
	            6505   8767   5436   7896
	        </span>
	      </div>
	      <div class="valid">
	       <img src="assets/images/ic_valid_thru.svg" alt=""> 
	       <span id ="cardexpriy1">
	           07/25
	       </span> 
	      </div>
	      <div class="bottom-name">
	       SAMEER  CHAKRABORTHY
	      </div>
	    </div>

	    <a  onclick="show('debitUpload')" style="margin-right:12px;margin-top:4px;text-decoration: underline;float:right">Change photo</a>
	    <br><br>
	    <!--<input type="hidden" id="outvalue"> -->
		 <p id="outvalue" hidden></p>
	    <!-- <input type="button" style="height:50px;" class="btn btn-next" onclick="show('TermsandCond')" value="CONFIRM"> -->
	    <input type="button"  style="height:50px;" class="btn btn-next finalSubmit"  value="CONFIRM">

	    <img id="confirmPerview">

	  </div>

	</div>

	<div id="uploadedDebit1" style="display:none;">
	  <div class="container">
	    <br>
	    <h4>Upload your image</h4>
	    <br>


	    <div class="contain">
	      <img src="assets/images/dynamic_101.svg" alt="">
	      <div class="top-right12">
	      <img src="assets/images/101Crop.png" style="height:70px;width:75px;" alt="" id="passout1">
	      </div>

	      <div class="centered">
	        <span id="cardno1x1_01">
	            6505   8767   5436   7896
	        </span>
	      </div>

	      <div class="bottom-name">
	        SAMEER
	      </div>
	      <div class="valid">
	        <span id="cardexpriy1x1_01">
	             07/25
	        </span>
	       
	      </div>
	    </div>


	    <a onclick="show('debitUpload')"  style="margin-right:12px;margin-top:4px;text-decoration: underline;float:right">Change photo</a>
	    <br><br> <br>
	    <input type="button" style="height:50px;" class="btn btn-next finalSubmit"  value="CONFIRM">

	  </div>

	</div>
	<div id="finalSuccess" style="display:none;">
	    <br>
	    <div class="container">
	        <h4>Thank you</h4>
	        <h5 style="color:orange">Successfully submitted your design</h5>
	        
	    <br> 

	        <h6 style="line-height: unset;font-weight: 400;">Fincare bank will review your design and send you a confirmation email within 2 business day.</h6>
	    <br>
	        <h6 style="line-height: unset;font-weight: 400;">Uploaded images with third party copyrighted content will not be approved.</h6>
	    <br>
	        <h6 style="line-height: unset;font-weight: 400;">Once your design is approved, your new card will then arrrive within 5 to 7 business days</h6>

	    <br><br><br>
	        <input type="button" style="height:50px;" class="btn btn-next" onclick="show('TermsandCond')" value="Go To Home">
	    </div>
	</div>

	<div id="videocat" class="modal fade in" role="dialog">
	  <div class="modal-dialog">
	      <div class="modal-content">
	          
	            
	            <video id="vid"></video>
	            <button id="snap" class="btn btn-primary col-md-4 offset-md-4">Take a Snapshot</button>
	            <br>
	          
	      </div>
	  </div>
	</div>

	<input type="hidden" id="success">
</div>

<script>
    let selectArr=[];
function account()
{
    $("#show_loader").show();
     //$('body').css("opacity",0.5);
    let addUserData={        
        cif:cif
    }
    $.ajax({
        type: 'POST',        
        url: 'http://localhost/ssp/ssp_u/photodebitcard/libraries/CommonLibrary.php?function=AccountList',
        data: { data : encryptData(addUserData) },
        dataType: 'json',
        success: function (response) {
           // console.log("response:"+response);
            var $select = $('#account'); 
               // $select.find('option').remove();  
                
                selectArr =response;
                $.each(response,function(k, v) 
                {
                    $select.append('<option value=' + v.text + '>' + v.value + '</option>');
                });
                //$('body').css("opacity",0);
                $("#show_loader").hide();
             //alert("select:"+$select.val());
          
        }, 
        error: function(xhr, status, error) {
            alert(xhr.status);
            if(xhr.status == 403){
                window.location.href = site_url+"welcome/error/403";
            }else{
              
            }
        }
    });  
}
function cardlist()
{
    $("#show_loader").show();
    let addUserData={        
        cif:cif
    }
    $.ajax({
        type: 'POST',        
        url: 'http://localhost/ssp/ssp_u/photodebitcard/libraries/CommonLibrary.php?function=cardlist',
        data: { data : encryptData(addUserData) },
        dataType: 'json',
        success: function (response) {
          console.log("array"+response);
          var obj4=  JSON.stringify(response);
            var res4= JSON.parse(obj4);
            //alert(window.textvalue);
          $("#show_loader").hide();
            var data = [];
            data.push(res4.last);
           console.log("response123:"+data);
           console.log(obj4.includes(window.textvalue));
           $('#myAlert2').hide();
            if(obj4.includes(window.textvalue)) {

              $('#myAlert2').hide();
                $('#success').val("1");
                $('#cardValidate').prop('disabled',false);
                var chuncks = res4.cardno.match(/.{1,4}/g);
                var new_value = chuncks.join(" ");
                if($('#photovalue') == 1)
                {
                    $('#cardno').text(new_value);
                    $('#cardno1').text(new_value);
                    $('#cardexpriy').text(res4.date);
                    $('#cardexpriy1').text(res4.date); 
                }
                if($('#photovalue') == 2)
                {
                    $('#cardno1x1').text(new_value);
                    $('#cardno1x1_01').text(new_value);
                    $('#cardexpriy1x1').text(res4.date);
                    $('#cardexpriy1x1_01').text(res4.date); 
                }
                

             }
             else{
              $('#myAlert2').show();
               $('#success').val("2")
               $('#cardValidate').prop('disabled',true);
             }

        }, 
        error: function(xhr, status, error) {
            alert(xhr.status);
            if(xhr.status == 403){
                window.location.href = site_url+"welcome/error/403";
            }else{
                //kendo.ui.progress($("#tds-form"), false);
                //showErrorMsg('Something Went Worng..',$("#tds-form") );
            }
        }
    });  
}

    var cif = "190000135346" ;
    var cust_name = "Cust4" ;
            $('#photo1').click('on',function()
            {
                $('#photovalue').val('1');
            })
            $('#photo2').click('on',function()
            {
                $('#photovalue').val('2');
            });
           
            $('#cardValidate').prop('disabled',true);
            $('#nextUpload').prop('disabled', true);
            $('#account').on('change',function(){
                var sel = $(this).val();
                console.log('selectarray:'+selectArr);
                const result = selectArr.find( ({ value }) => value === sel );
                console.log(result.balance);
                if(result.balance <=590.00)
                {
                    alert('Please select sufficient balance account number.')
                }

               

            })
            
            $('#cardValidate').click('on',function()
            {

                    if (!$('#confirmCheckBox').is(":checked")) {
                         $('#cardValidate').prop('disabled',true);
                        alert('Please read and accept Terms and Condition');
                        // break;
                    } 
                    else
                    {
                        $('#cardValidate').prop('disabled',false);
                        show('debitUpload');
                    }
            
          })
          $('.finalSubmit').click(function() {

			//Initialize();
            $("#show_loader").show();
			 let addUserData={        
					cif:cif
				}
				$.ajax({
					type: 'POST',        
					url: 'http://localhost/ssp/ssp_u/photodebitcard/libraries/CommonLibrary.php?function=Initialize',
					data: { data : encryptData(addUserData) },
					dataType: 'json',
					success: function (response) {
					  var obj=	JSON.stringify(response);
						var res= JSON.parse(obj);
							console.log("test->"+res.ReferenceNo);
							$('#referno').text(res.ReferenceNo);
							let addUserData1={        
								cif:cif,
								refer:res.ReferenceNo,
								image: $('#outvalue').val()
							}
							console.log("dataimage:"+addUserData1.image);
							$.ajax({
								type: 'POST',        
								url: 'http://localhost/ssp/ssp_u/photodebitcard/libraries/CommonLibrary.php?function=Dmsinsert',
								data: { data : encryptData(addUserData1) },
								dataType: 'json',
								success: function (response1) {
									console.log('final'+response1);
									var obj1= JSON.stringify(response1);
									var res1 = JSON.parse(obj1);
									console.log('obj1'+res1.status);
									if(res1.status == 'Success')
									{
										let addUserData2={        
											  cif:cif,
											  name:cust_name,
											  phoneno:'9036903662',
											  accountno:$('#account').val(),
											  refer:res.ReferenceNo,
											  cardno:window.textvalue,
                                              photo_type:$('#photovalue').val(),
											  
										}
          										  $.ajax({
          											  type: 'POST',        
          											  url: 'http://localhost/ssp/ssp_u/photodebitcard/libraries/CommonLibrary.php?function=InsertData',
          											  data: { data : encryptData(addUserData2) },
          											  dataType: 'json',
          											  success: function (response2) {
          													console.log('final'+response2);
          													var obj2= JSON.stringify(response2);
          													var res2 = JSON.parse(obj2);
          													console.log('obj2'+res2.status);
          													if(res2.code == 1)
          													{
                              														//$('').hide
                                                                           
                                                                  let addUserData3={ 
                                                                  cif:cif, 
                                                                  accountno:$('#account').val(),                            
                                                                }
                                                                $.ajax({
                                                                    type: 'POST',        
                                                                    url: 'http://localhost/ssp/ssp_u/photodebitcard/libraries/CommonLibrary.php?function=GLDebit',
                                                                    data: { data : encryptData(addUserData3) },
                                                                    dataType: 'json',
                                                                    success: function (response3) {

                                                                      // mobile number
                                                                          let addUserData4={ 
                                                                                
                                                                                mobile:'9036903662',                            
                                                                              }
                                                                              $.ajax({
                                                                                  type: 'POST',        
                                                                                  url: 'http://localhost/ssp/ssp_u/photodebitcard/libraries/CommonLibrary.php?function=SendSMS',
                                                                                  data: { data : encryptData(addUserData4) },
                                                                                  dataType: 'json',
                                                                                  success: function (response4) {

                                                                            //                  let addUserData3={ 
                                                                            //           refer:res.ReferenceNo,                                
                                                                            //         }
                                                                            //         $.ajax({
                                                                            //   type: 'POST',        
                                                                            //   url: 'http://localhost/ssp/ssp_u/photodebitcard/libraries/CommonLibrary.php?function=DMSRETRIEVE',
                                                                            //   data: { data : encryptData(addUserData3) },
                                                                            //   dataType: 'json',
                                                                            //   success: function (response3) {
                                                                            //     console.log('final123456: '+response3);
                                                                            //     var obj3= JSON.stringify(response3);
                                                                            //     var res3 = JSON.parse(obj3);
                                                                            //     console.log('obj3'+res3);
                                                                            //     $('#confirmPerview').attr('src',res3);
                                                                              
                                                                            //             let a = document.createElement("a");
                                                                            //         a.href = res3;
                                                                            //         a.download = "syam.jpg";
                                                                            //         // a.href = "data:application/octet-stream;base64,"+res3;
                                                                            //         //a.download = "tds.pdf"
                                                                            //         a.click();
                                                                            //   },  
                                                                            //   error: function(xhr, status, error) {
                                                                            //     alert(xhr.status);
                                                                            //     if(xhr.status == 403){
                                                                            //       window.location.href = site_url+"welcome/error/403";
                                                                            //     }else{
                                                                            //       //kendo.ui.progress($("#tds-form"), false);
                                                                            //       //showErrorMsg('Something Went Worng..',$("#tds-form") );
                                                                            //     }
                                                                            //   }
                                                                            // });  



                                                                                  $("#show_loader").hide(); 
                                                                                    show('cardDetails');
                                                                                  },  
                                                                                  error: function(xhr, status, error) {
                                                                                    //alert(xhr.status);
                                                                                    if(xhr.status == 403){
                                                                                      window.location.href = site_url+"welcome/error/403";
                                                                                    }else{
                                                                                      //kendo.ui.progress($("#tds-form"), false);
                                                                                      //showErrorMsg('Something Went Worng..',$("#tds-form") );
                                                                                    }
                                                                                  }
                                                                                });  
                                                                    },  
                                                                    error: function(xhr, status, error) {
                                                                     if(xhr.status == 403){
                                                                          $("#show_loader").hide();
                                                                          alert("403 Forbidden error");
                                                                        window.location.href = "http://localhost/ssp/spp_u/photodebitcard";
                                                                        }else{
                                                                            $("#show_loader").hide();
                                                                            alert("Something Went Worng..");
                                                                       
                                                                      }
                                                                    }
                                                                  });  
                                                                               
														                          
														                            //$('#finalSuccess').show();
													                          }
                                                    else
                                                    {
                                                        alert(res2.status);
                                                        $("#show_loader").hide();
                                                    }
												
											  }, 
											  error: function(xhr, status, error) {
												    //alert(xhr.status);
												    if(xhr.status == 403){
                                                        $("#show_loader").hide();
                                                        alert("403 Forbidden error");
													  window.location.href = "http://localhost/ssp/spp_u/photodebitcard";
												    }else{
                                                        $("#show_loader").hide();
                                                        alert("Something Went Worng..");
													  //kendo.ui.progress($("#tds-form"), false);
													  //showErrorMsg('Something Went Worng..',$("#tds-form") );
												  }
											  }
										  });  
									}
									else
									{
                                        $("#show_loader").hide();
										alert(res.msg);
										//$('#error-dms').text(res.msg);
										show('authentication');
												   
									}


								}, 
								error: function(xhr, status, error) {
									//alert(xhr.status);
									if(xhr.status == 403){
										window.location.href = site_url+"welcome/error/403";
									}else{
										//kendo.ui.progress($("#tds-form"), false);
										//showErrorMsg('Something Went Worng..',$("#tds-form") );
									}
								}
							});  

					}, 
					error: function(xhr, status, error) {
						alert(xhr.status);
						if(xhr.status == 403){
							window.location.href = site_url+"welcome/error/403";
						}else{
							kendo.ui.progress($("#tds-form"), false);
							showErrorMsg('Something Went Worng..',$("#tds-form") );
						}
					}
				});  				
	      $('#videocat').on('hidden.bs.modal', function (e) {
                console.log("Modal hidden");
                const vid = document.querySelector('video');
                navigator.mediaDevices.getUserMedia({video: false});
                
             });			
            

   

    });
 

function video_cat(){
    $('#videocat').modal('show'); 
        const vid = document.querySelector('video');
        navigator.mediaDevices.getUserMedia({video: true}) // request cam
        .then(stream => {
        vid.srcObject = stream; // don't use createObjectURL(MediaStream)
        return vid.play(); // returns a Promise
        })
        .then(()=>{ // enable the button
        const btn = document.getElementById('snap');
        btn.disabled = false;
        btn.onclick = e => {
        takeASnap()
        .then(download);
        };
    })
    .catch(e=>console.log('please use the fiddle instead'));

}

function show_modal(){
  $('#errorModal').modal(); 
}

function toggleCheckBox()
{
  if (document.getElementById('confirmCheckBox').checked){
    
    $("#consentMessage").attr("style","font-size:16px;text-align:justify;color: black");
    $("#cancel_btn").attr("style","margin:0 auto; display:block; color:white;background-color: #A8060A;width:100%");
    $("#cancel_btn").removeAttr("disabled");
    $("#cnf_btn").attr("style","margin:0 auto; display:block; color:white;background-color: #A8060A;width:100%");
    $("#cnf_btn").removeAttr("disabled");
    $("#finger_print").attr("src","assets/images/4.svg")
    $("#fingerPrintWell").attr("style","text-align:center; margin:0 auto; display:block;padding:0px; background-color: #fff7e8;border: 1px solid #fcb316;");
    $("#IRIS").attr("src","assets/images/3.svg")
    $("#fingerPrintWell_1").attr("style","text-align:center; margin:0 auto; display:block; padding:0px;background-color: #fff7e8;border: 1px solid #fcb316;");
    if(($('#account').val()!="") && ($('#inputs4').val()!=""))
    {
      $('#cardValidate').prop('disabled',false);
    }
    else
    {
       $('#cardValidate').prop('disabled',true);
    }
  
  } else {
     $('#cardValidate').prop('disabled',true);
      $("#consentMessage").attr("style","font-size:16px;text-align:justify;color: gray");
      $("#cancel_btn").attr("style","margin:0 auto; display:block;color: #3D3D3D; background-color: #EEE;border: 0px solid #999;width:100%;");
      $("#cancel_btn").attr("disabled","disabled");
      $("#cnf_btn").attr("style","margin:0 auto; display:block;color: #3D3D3D; background-color: #EEE;border: 0px solid #999;width:100%;");
      $("#cnf_btn").attr("disabled","disabled");
      $("#finger_print").attr("src","assets/images/1.svg")
      $("#fingerPrintWell").attr("style","text-align:center; margin:0 auto; display:block; padding:0px;background-color: white;border: 1px solid gray;");
      $("#IRIS").attr("src","assets/images/2.svg")
      $("#fingerPrintWell_1").attr("style","text-align:center; margin:0 auto; display:block; padding:0px;background-color: white;border: 1px solid gray;");

    }
}

function updateItems(delta)
{
    var $items = $('#group').children();
    var $current = $items.filter('.current');
    $current = $current.length ? $current : $items.first();
    var index = $current.index() + delta;
    // Range check the new index
    index = (index < 0) ? 0 : ((index > $items.length) ? $items.length : index); 
    $current.removeClass('current');
    $current = $items.eq(index).addClass('current');
    // Hide/show the next/prev
    // $("#prev").toggle(!$current.is($items.first()));    
    // $("#next").toggle(!$current.is($items.last()));    
}
function show(elementId,type) 
{ 
    if(type){
    sessionStorage.setItem('type', type);
  }
    document.getElementById("authentication").style.display="none";
    document.getElementById("cardDetails").style.display="none";
    document.getElementById("TermsandCond").style.display="none";
    document.getElementById("resetPin").style.display="none";
    document.getElementById("debitUpload").style.display="none";
    document.getElementById("debitUpload1").style.display="none";
    document.getElementById("uploadedDebit").style.display="none";
    document.getElementById("uploadedDebit1").style.display="none"; 
    // document.getElementById("101Cross").style.display="none"; 
    document.getElementById(elementId).style.display="block";
    if(elementId === 'debitUpload') {
    if(sessionStorage.getItem('type') === 'e2e'){
      document.getElementById("debitUpload1").style.display="none";
      document.getElementById(elementId).style.display="block";
    } else {
      document.getElementById("debitUpload").style.display="none";
      document.getElementById("debitUpload1").style.display="block";
    }
  }
  if(elementId === 'uploadedDebit') {
    if(sessionStorage.getItem('type') === 'e2e'){
      document.getElementById("uploadedDebit1").style.display="none";
      document.getElementById(elementId).style.display="block";
    } else {
      document.getElementById("uploadedDebit").style.display="none";
      document.getElementById("uploadedDebit1").style.display="block";
    }
  }
     if(elementId == "authentication")
    {
        location.reload();
    //window.location.href="http://localhost/ssp/ssp_u/ssp_home.php"
    } 
    if(elementId == "resetPin")
    {
        account();

    }
    //if(addEventListener)
}
function Initialize()
{
    $("#show_loader").show();
    let addUserData={        
        cif:cif
    }
    $.ajax({
        type: 'POST',        
        url: 'http://localhost/ssp/ssp_u/photodebitcard/libraries/CommonLibrary.php?function=Initialize',
        data: { data : encryptData(addUserData) },
        dataType: 'json',
        success: function (response) {
          var obj=	JSON.stringify(response);
      		var res1= JSON.parse(obj);
                console.log("test->"+res1.ReferenceNo);
      			$('#referno').text(res1.ReferenceNo);
              $("#show_loader").hide();

        }, 
        error: function(xhr, status, error) {
            //alert(xhr.status);
            $("#show_loader").hide();
            if(xhr.status == 403){
                window.location.href = site_url+"welcome/error/403";
            }else{
                //kendo.ui.progress($("#tds-form"), false);
                //showErrorMsg('Something Went Worng..',$("#tds-form") );
            }
        }
    });  
}

function onClickHandler(ev) {
    var el = window._protected_reference = document.createElement("INPUT");
    el.type = "file";
    el.accept = "image/jpg";
    el.multiple = "multiple"; // remove to have a single file selection

    // (cancel will not trigger 'change')
    el.addEventListener('change', function(ev2) 
    {
    // access el.files[] to do something with it (test its length!)
     // file size
        var file =el.files[0].type;
        //alert("file : "+file);
        var fileExtension = file.split("/").pop();
       // alert("fileExtension: "+fileExtension);

        var _validFileExtensions = ["jpg", "jpeg", "bmp", "gif", "png"];  
        for (var j = 0; j < _validFileExtensions.length; j++) {
                var sCurExtension = _validFileExtensions[j];
                if ( fileExtension== sCurExtension.toLowerCase()) {
                   var blnValid = true;
                    //alert("sCurExtension: "+sCurExtension)
                    break;
                }
            }
             
            if (!blnValid) {
                alert("Sorry, " + fileExtension + " is invalid, allowed extensions are: " + _validFileExtensions.join(", "));
                //oInput.value = "";
                blnValid=false;
            }

        if(blnValid)
        {



            var min =parseInt(el.files[0].size/ 1024);
            var max =parseInt(el.files[0].size/ 1024/1024);
    
   
            if(min < 300)
            {
                alert('Minimum is 300KB. You file size is: ' + min +' KB');
                $('#nextUpload').prop('disabled',true);
            }else if(max > 5){
                alert('Maximum is 5MB. You file size is: ' + max +' MB');
                $('#nextUpload').prop('disabled',true);
            }
            else
            {

           
            // add first image, if available
                if (el.files.length) 
                {
            		if (el.files && el.files[0]) 
                    {

                        var reader = new FileReader();
                        reader.onload = function(e) 
                        {
                            // $('#out').attr('src', e.target.result);
                            // $('#output2').attr('src', e.target.result);
                           // $('#confirmPerview').attr('src', e.target.result);
            			            
                            console.log("url"+e.target.result);
                            console.log("image:"+e.target.result);
                            var img = document.createElement("img");
                            img.onload = () => {

                                //alert("width"+img.width);
                                //alert("height"+img.height);

                              
                              /* Start corp image */
                                if($('#photovalue').val() == 1)
                                {
                                    if(img.width <=640 && img.height <=480)
                                    {
                                        alert("The image resolution is too low.");
                                    $('#nextUpload').prop('disabled',true);
                                    }
                                    else
                                    {
                                        $('#fullPreview').attr('src', e.target.result); 
                                        $('#fullConfirmPreview').attr('src', e.target.result); 
                                        $('#fullPreview').height(205).width(328);
                                        $('#fullConfirmPreview').height(205).width(328);
                                        $('#nextUpload').prop('disabled',false);
                                    }
                                //  var width = 327;
                                 //var  height =204;

                                }
                                if($('#photovalue').val() == 2)
                                {
                                    if(img.width <=400 && img.height <=400)
                                    {
                                        alert("Please Upload Image should be greater than 400 x 400 px");
                                        $('#nextUpload').prop('disabled', true);
                                    }
                                    else
                                    {
                                        $('#passout').attr('src', e.target.result);                                      
                                        $('#passout').height(70).width(75);
                                        $('#passout1').attr('src', e.target.result);  
                                        $('#passout1').height(70).width(75);
                                        $('#nextUpload').prop('disabled', false);
                                    }

                                }
                            }
                            // $('#out').attr('src', e.target.result);
                            // $('#output2').attr('src', e.target.result);
                             $("#outvalue").val(e.target.result);
                            img.src = e.target.result;
                        }
                        reader.readAsDataURL(el.files[0]); // convert to base64 string
                    }       
                  
                }

            }
        }
    // test some async handling
    new Promise(function(resolve) {
      setTimeout(function() { console.log(el.files); resolve(); }, 1000);
    })
    .then(function() {
      // clear / free reference
      el = window._protected_reference = undefined;
    });

    });

    el.click(); // open
}

function resizeCrop( src, width, height ){

        
    



     var crop = width == 0 || height == 0;
    // // not resize
    if(src.width <= width && height == 0) {
         width  = src.width;
     }
    // // resize
	if( src.width > width && height == 0){
         height = src.height * (width / src.width);
         height = src.height;
     }


    // // check scale
     var xscale = width  / src.width;
     var yscale = height / src.height;
     var scale  = crop ? Math.min(xscale, yscale) : Math.max(xscale, yscale);
    // // create empty canvas
     var canvas = document.createElement("canvas");                  
     canvas.width  = width ? width   : Math.round(src.width  * scale);
     canvas.height = height ? height : Math.round(src.height * scale);
     canvas.getContext("2d").scale(scale,scale);
    // // crop it top center
     canvas.getContext("2d").drawImage(src, ((src.width * scale) - canvas.width) * -.5 , ((src.height * scale) - canvas.height) * -.5 );
    return canvas;
}

let pin = "";
let num = "";
var myArray =[];
 $(function() {
    $(".inputs").keyup(function (e) {  
         
    
      formatString(e);
      
     
        if (this.value.length == this.maxLength) {
          $(this).next('.inputs').focus();
            if($('#inputs4').val=="")
            {
              alert("Please enter last four digit debitcard number");
            }

      
          
        }
        
    });
    $('#inputs4').keyup(function(){
      console.log("this1->"+$('#inputs1').val());
      console.log("this2->"+$('#inputs2').val());
      console.log("this3->"+$('#inputs3').val());
      console.log("this4->"+this.value);
      window.textvalue=$('#inputs1').val()+$('#inputs2').val()+$('#inputs3').val()+this.value;
      cardlist();
      console.log("yest->"+window.textvalue);
    });
}); 

function formatString(e) {
    var inputChar = String.fromCharCode(event.keyCode);
    var code = event.keyCode;
    var allowedKeys = [8];
    if (allowedKeys.indexOf(code) !== -1) {
        return;
    }

    event.target.value = event.target.value.replace(
   
    /^([0]+)\/|[0]+$/g, '0' // 0/ > 0 and 00 > 0
    ).replace(
    /[^\d\/]|^[\/]*$/g, '' // To allow only digits and `/`
    ).replace(
    /\/\//g, '/' // Prevent entering more than 1 `/`
    );
}


function takeASnap(){
    const canvas = document.createElement('canvas'); // create a canvas
    const ctx = canvas.getContext('2d'); // get its context
    canvas.width = vid.videoWidth; // set its size to the one of the video
    canvas.height = vid.videoHeight;
    ctx.drawImage(vid, 0,0); // the video
    return new Promise((res, rej)=>{
    canvas.toBlob(res, 'image/jpeg'); // request a Blob from the canvas
    });
}
function download(blob){
    // uses the <a download> to download a Blob
    let a = document.createElement('a'); 
    a.href = URL.createObjectURL(blob);
    a.download = 'screenshot.jpg';
    document.body.appendChild(a);
    a.click();
}

</script>
</body>
</html>
