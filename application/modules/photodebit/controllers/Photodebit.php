<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Photodebit extends MY_Controller {

	public function __construct()
    {
            parent::__construct();
           
           	if (!isset($this->session->loginStatus) ) {
				
					redirect('login');
			}
            
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                if(isset($_POST['data'])){
                    $this->load->library("Customencryption");
                    $this->customencryption->decryptPOSTRequest($_POST['data']);
                }
           }
           
            
            //Your own constructor code
    }
	public function index()
	{
		$data['title'] = "Photo Debit Card";
		$data['header'] = 'layout/header';
		$data['page'] = 'index';
		$data['footer'] = 'layout/footer';
		$data['photo'] = FALSE;
		$this->load->view('template',$data);
		//$this->load->view('index');
	}
}
