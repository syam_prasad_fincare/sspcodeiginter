
$(document).ready(function () {
    
    //nav-dropdown
    $('.nav-dropdown > .nav-tabs > li > a').click(function() {
        var text = $(this).html();
        //$(this).closest('.nav-dropdown').find('.btn-dropdown').html(text);   
        $(this).parent().parent().prev('.btn-dropdown').html(text); 
        $(this).parents('.nav-tabs').removeClass('in');        
        $(this).parent().siblings('li').removeClass('active');
    });

    //nav-tabs-togglable
    $(document).on('click', '.nav-togglable .nav-tabs > li.active > a', function() {
        var $parent = $(this).parent();
        // Ensure link isn't just a dropdown menu link     
        if (!$parent.hasClass('dropdown')) {
            // Deactivate tab buttons
            $('.nav-togglable li').removeClass('active');
            // Hides tab contents
            var tabName = $(this).attr('href');
            $(tabName).removeClass('active').addClass('fade');
        }
    }); 
    

    /*==========Scrollbar===========*/
    // function scrollBar() {
    //     $('.scrollbar-theme').jScrollPane();               
    // }
    // scrollBar();    
    /*==========//Scrollbar===========*/
    
    
    /*========== HEADER ===========*/
    //Decreasing a fixed navbar's height after scroll and Affixing navbar after scroll, sticky navbar
     var navbarOffest = $('.header .navbar').offset().top;// Calculating the distance from the top of the page to the initial position of the navbar, and store it in a variable
     $(window).on('scroll', function () {
         var navbarHeight = $('.header .navbar').outerHeight();
         if ( $(window).scrollTop() >= navbarOffest ) {
             $('.header .navbar').addClass('navbar-fixed-top');
            //  $('.header .navbar').addClass('compressed');
             $('body').css('padding-top', navbarHeight + 'px');
         } else {
             $('.header .navbar').removeClass('navbar-fixed-top');
            //  $('.header .navbar').removeClass('compressed');
             $('body').css('padding-top', '0');
         }
     });
    
    $(document).on('click', '.yamm .dropdown-menu', function(e) {
       e.stopPropagation()
    })
    /*========== //HEADER ===========*/
    
    
    
    /*========== FOOTER ===========*/
    //sticky footer
    var footerHeight = $('.footer').outerHeight();
    $('body').css('padding-bottom', footerHeight);
    
    /*========== //FOOTER ===========*/    


    /*========== sidenav dropdown ===========*/  
    
     /* Loop through all dropdown buttons to toggle between hiding and showing its dropdown content - This allows the user to have multiple dropdowns without any conflict */
     var dropdown = $(".dropdown-btn");
     var i;

     for (i = 0; i < dropdown.length; i++) {
        dropdown[i].addEventListener("click", function() {
            this.classList.toggle("active");
            var dropdownContent = this.nextElementSibling;
            if (dropdownContent.style.display === "block") {
                dropdownContent.style.display = "none";
            } else {
                dropdownContent.style.display = "block";
            }
            for(var ab = 0; ab<$(this).siblings().length;ab++){
                if($(this).siblings()[ab] == $(this) || $(this).siblings()[ab] == this.nextElementSibling){
                }
                else{
                    $($(this).siblings()[ab]).removeClass("active").removeClass("open");
                    if($($(this).siblings()[ab]).hasClass("dropdown-container")){
                    $($(this).siblings()[ab]).hide();
                    }
                }
            }
        });
     }
     
     $('.menu').click(function(){
        $('.menu').removeClass('open');
        $('.submenu').removeClass('open');
        $(this).addClass('open');
        if($(".menu").hasClass('open')){
            $('.submenu').click(function(){
                $('.submenu').removeClass('open');
                $('.menu').removeClass('open');
                $(this).addClass('open');
            })
        }
        else{
            $('.submenu').removeClass('open');
            $('.menu').removeClass('open');
        }
     });

    /*========== //sidenav dropdown ===========*/  
    
    /*========== button ===========*/  
    $(".Button").kendoButton();
     
    /*==========// button ===========*/  

    $(".kendoDropDown").kendoDropDownList(); //kendodropdown

    datepicker = $(".KendoDatepicker").kendoDatePicker({ format: "dd-MM-yyyy",   disableDates: function (date) {
        return date > new Date(currentServerDate);
    } }); // kendo date picker
    $(".KendoDatepicker").bind("focus", function(){
        $(this).data("kendoDatePicker").open();                    
    });
       
    if ($(window).width() < 767) {
    
    
        $(document).mouseup(function(e) 
        {
            var container = $("#notification-panel");
    
            // if the target of the click isn't the container nor a descendant of the container
            if (!container.is(e.target) && container.has(e.target).length === 0) 
            {
                container.hide();
            }
        });//close notification on click outside
    
    
        $(document).mouseup(function(e) 
        {
            var container = $("#mySidenav");
    
            // if the target of the click isn't the container nor a descendant of the container
            if (!container.is(e.target) && container.has(e.target).length === 0) 
            {
                container[0].style.width = "0px";
                //container[0].removeClass('sideNavWidth');
                // $('#mySidenav').removeClass('sideNavWidth');
            }
            if($('.navbar-collapse').hasClass('in')){
                $('.sidenav').css("padding-top","130px");
            }else{
                $('.sidenav').css("padding-top","85px");
            }
        }); //close sidenav on click outside
    
        // $(document).mouseup(function(e) 
        // {
        //     var container = $("#bs-example-navbar-collapse-1");

        //     // if the target of the click isn't the container nor a descendant of the container
        //     if (!container.is(e.target) && container.has(e.target).length === 0) 
        //     {
        //         container.removeClass('in');
        //     }
        // }); //close sidenav on click outside
    }

    // setTimeout(function(){ 
        $('.scrollbar-theme').jScrollPane(
            {autoReinitialise: true}
        ); 
    // }, 1000);
    
  
    
    $('[data-toggle="tooltip"]').tooltip();  //tooltip

    $('#openNav').click(function () { 
        // document.getElementById("mySidenav").style.width = "250px";
        $('#mySidenav').toggleClass('sideNavWidth');
    });   

    $('#toggleNotification').click(function () {
		$('#notification-panel').toggle();
		$.ajax({
			url: site_url + 'dashboard/viewedNotifications',
			dataType: "json",
			type: 'post',
			success: function (response) {
				if (response.code===0){
					$(".badge").hide();
				}
    		}    
		});
	});
});
