//function formatDate(date){
//
//    var monthNames = [ "January", "February", "March", "April", "May", "June",
//    "July", "August", "September", "October", "November", "December" ];
//
//    var newDate = new Date(date);
//    var formattedDate = newDate.getDate()+' '+monthNames[newDate.getMonth()] + ' ' + newDate.getFullYear();
//    return formattedDate;
//}
function formatToInr(num)
{    
    var y=Number(parseFloat(num).toFixed(2)).toLocaleString('en-IN', {
    minimumFractionDigits: 2
    });
    console.log(y);
    return y;
}



function formatDate(date){
   // 01, 02, 03, ... 29, 30, 31
   var newDate = new Date(date);
   var dd = (newDate.getDate() < 10 ? '0' : '') + newDate.getDate();
   // 01, 02, 03, ... 10, 11, 12
   var MM = ((newDate.getMonth() + 1) < 10 ? '0' : '') + (newDate.getMonth() + 1);
   // 1970, 1971, ... 2015, 2016, ...
   var yyyy = newDate.getFullYear();

   // create the format you want
   return (dd + "-" + MM + "-" + yyyy);
}

function formatDateTime(date){
    console.log("date:"+date);
    // 01, 02, 03, ... 29, 30, 31
    var newDate = new Date(date);
    var dd = (newDate.getDate() < 10 ? '0' : '') + newDate.getDate();
    // 01, 02, 03, ... 10, 11, 12
    var MM = ((newDate.getMonth() + 1) < 10 ? '0' : '') + (newDate.getMonth() + 1);
    // 1970, 1971, ... 2015, 2016, ...
    var yyyy = newDate.getFullYear();
 
    var hours = newDate.getHours();
    var minutes = newDate.getMinutes();
    var ampm = hours >= 12 ? 'PM' : 'AM';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0'+minutes : minutes;
    var strTime = hours + ':' + minutes + ' ' + ampm;
    return dd + "-" + MM + "-" + yyyy + "  " + strTime;
    // create the format you want
    return (dd + "-" + MM + "-" + yyyy);
 }

var downloadTimer;
var timeleft = 120;
function timerCountdown(ID){
    timeleft =120;
    clearInterval(downloadTimer);
   // $(ID+" #resendMSG").addClass("show");
    $(ID+" #resendTime").html(timeleft + " seconds");
    downloadTimer = setInterval(function(){
    timeleft -= 1;
    //$(ID+" #resendMSG").addClass("show");
    $(ID+" #resendTime").html(timeleft + " seconds");
    if(timeleft <= 0){
        clearInterval(downloadTimer);
        timeleft = 0;
        $(ID+" #resendBtn").removeClass("d-none");
        $(ID+" #resendMSG").addClass("d-none");
    }
    }, 1000);
}

function addError(id,errID,msg,error = true){
        
    if(error){
        $(errID).css("color","red");
        $(errID).css("font-size",'14px');
        $(errID).html(msg);
    }else{
        $(errID).css("color","#737373");
        $(errID).html(msg);
        $(errID).css("font-size",'12px');
    }
}

// function encryptData(param){
//      return  req = CryptoJS.AES.encrypt(JSON.stringify(param), encryption_key, {format: CryptoJSAesJson}).toString();
// }

// function decryptData(encrypted){
//      return  JSON.parse(CryptoJS.AES.decrypt(encrypted, encryption_key, {format: CryptoJSAesJson}).toString(CryptoJS.enc.Utf8));
// }
function encryptData(string) 
{
   var json= JSON.stringify(string);
  // alert(json);
    var iv = CryptoJS.lib.WordArray.random(16);// the reason to be 16, please read on `encryptMethod` property.
//alert(iv);
   // var salt = CryptoJS.lib.WordArray.random(256);//alert(salt);//var hash = CryptoJS.SHA256("Message");alert(hash)
   // var sl= CryptoJS.enc.Hex.parse(salt);
    var iterations = 500;
    //var encryptMethodLength = 64;// example: AES number is 256 / 4 = 64
//alert((encryptMethodLength()/8));
//alert(encryption_key);
//var iterations = 999;
    var keySize = 256;
    var salt = CryptoJS.lib.WordArray.random(128/8);

    console.log(salt.toString(CryptoJS.enc.Base64));

    var hashKey = CryptoJS.PBKDF2(encryption_key, salt, {'hasher': CryptoJS.algo.SHA512,
        keySize: keySize/32,
        iterations: iterations
    });console.log(hashKey.toString(CryptoJS.enc.Base64));
 //console.log(hashKey.toString());
  //  var tes = CryptoJS.algo.SHA512.create();alert(tes);
    //var hashKey = CryptoJS.PBKDF2(encryption_key, salt, {'hasher': CryptoJS.algo.SHA512, 'keySize': (256/8), 'iterations': iterations}).toString();;
   // var hashKey = Crypto.PBKDF2(encryption_key,salt,32, {hasher:Crypto.SHA256, iterations:1000, asBytes:true});
    //
    //alert(hashKey+"qqq");
    var encrypted = CryptoJS.AES.encrypt(json, hashKey, {'mode': CryptoJS.mode.CBC, 'iv': iv});
 // alert(encrypted);
    var encryptedString = CryptoJS.enc.Base64.stringify(encrypted.ciphertext);
//alert(encryptedString+"Pavan");
    var output = {
        'ciphertext': encryptedString,
        'iv': CryptoJS.enc.Hex.stringify(iv),
        'salt':salt.toString(CryptoJS.enc.Base64),
        'salt2':CryptoJS.enc.Hex.stringify(salt),
        'iterations': iterations
    };
    //var decrypted = decipher.update(text.toString(),'hex','utf8') + decipher.final('utf8');
//alert(output);
    //alert(JSON.stringify(output));
    return  CryptoJS.enc.Base64.stringify(CryptoJS.enc.Utf8.parse(JSON.stringify(output)));
}// encrypt
function decryptData(encryptedString) {
    var json = JSON.parse(CryptoJS.enc.Utf8.stringify(CryptoJS.enc.Base64.parse(encryptedString)));

    var salt = CryptoJS.enc.Hex.parse(json.salt);
    var iv = CryptoJS.enc.Hex.parse(json.iv);

    var encrypted = json.ciphertext;// no need to base64 decode.

    var iterations = parseInt(json.iterations);
    if (iterations <= 0) {
        iterations = 999;
    }
    var encryptMethodLength = (this.encryptMethodLength/4);// example: AES number is 256 / 4 = 64
    var hashKey = CryptoJS.PBKDF2(encryption_key, salt, {'hasher': CryptoJS.algo.SHA512, 'keySize': (256/8), 'iterations': iterations});

    var decrypted = CryptoJS.AES.decrypt(encrypted, hashKey, {'mode': CryptoJS.mode.CBC, 'iv': iv});

    return decrypted.toString(CryptoJS.enc.Utf8);
}// decrypt

$(document).ready(function () {
	   
	let resetPassword = false;
    /**********  GD Start  **********/
    $.validator.addMethod("userpw",function(value,element){
                return this.optional(element) || /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,16}$/i.test(value);
            },"Passwords are 8-16 characters with uppercase letters, lowercase letters and at least one number.");
    // $.validator.addMethod( "userpw", function( value, element ) {
    //     return this.optional( element ) || /^[\w\s]+$/i.test( value );
    // }, "Passwords are 8-16 characters with uppercase letters, lowercase letters and at least one number." );
    // Bank & Corporate Login Step 1 
    $("#loginForm").validate({

        normalizer: function (value) {
            return $.trim(value);
        },
        rules: {
            userid: {
                required: true
            },
            userpw: {
                required: true,
                 userpw: true,
                //regex: /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,16}$/i
            },
            // corpid: {
            //     required: function () {
            //         if ($("#usertype").val() == "Corporate")
            //             return true;
            //         else
            //             false;
            //     }
            // },
            // captchaCode: {
            //     required: true
            // }
        },
        messages: {
            userid: {
                required: "Please enter valid user ID"
            },
            userpw: {
                required: "Please enter valid password",
                //regex: 'Passwords are 8-16 characters with uppercase letters, lowercase letters and at least one number.'
            },
            // corpid: {
            //     required: "Please enter valid corporate id"
            // },
            // captchaCode: {
            //     required: "Please enter valid captcha"
            // }
        },
        submitHandler: function (form) {
			let userid = $("#userid").val();
            let userpw = $("#userpw").val();
			//let usertype = $("#usertype").val();
			//let corpid = (usertype == "Corporate") ? $("#loginCorpId").data("kendoDropDownTree").value() : "1";
            //let captchaCode = $("#captchaCode").val();

            
            // login data
            let loginParam = {
                'userid': userid,
                'userpw': userpw,
                
                
            }
            let controller ='login';
            // console.log(loginParam);
            
            //kendo.ui.progress($("body"), true);
            
            
              $("#show_loader").show();
            // login request    
            $.ajax({
                url: site_url + controller + '/validateCredentials',
                dataType: "json",
                type: 'post',
                cors: true,
                
                // headers: {
                //     "Accept": 'application/json'
                // },
                data: { data : encryptData(loginParam) } ,
                success: function (response) {
                    console.log(response);
                      $("#show_loader").hide();
                    //kendo.ui.progress($("body"), false);
                    console.log(response[0].code);
                    if (response[0].code != 100) {
                       
                        if (response[0].code == "-2"){
                            //alert("sdfgsd");
                            resetPassword = true;
							$(".login_module").hide();
							timerCountdown(".forgot_pw_otp_module");
							$(".forgot_pw_otp_module").show();
							// showErrorMsg(response[0].status, $("#forgotPwOtpForm"));
                       }else{
                            showErrorMsg(response[0].status, form);
                       }
                    } else {
                        $("#otpForm #resendMSG").removeClass("d-none");
                        $("#otpForm #resendBtn").addClass("d-none");
                        timerCountdown("#otpForm");
                        showSuccessMsg(response[0].status, form);
                        $("#button1").attr('disabled','disabled');
                        //$("#button1").css('disabled');
                        $(".otp_form").removeClass("d-none" );
                    }
                },
                error: function(xhr, status, error) {
                    //alert(xhr.status);
                    // if(xhr.status == 403){
                    // //    window.location.href = site_url+"welcome/error/403";
                    // }else if(xhr.status == 502){
                    //    // window.location.href = site_url+"welcome/error/502";
                    // }else{
                    //   //  showErrorMsg(messages.ajaxerror.en, form);
                    // }
                }
            });

        }

    });
	
	


    // Bank & Corporate Login Step 2
    $("#otpForm").validate({

        normalizer: function (value) {
            return $.trim(value);
        },
        rules: {
            otpValue: {
                required: true
            }
        },
        messages: {
            otpValue: {
                required: "Please enter valid OTP"
            }
        }
        ,
        submitHandler: function (form) {

            let otpValue = $("#otpValue").val();
            let usertype = $("#usertype").val();

            // login data
            let loginParam = {
                'otpValue': otpValue
            }

           // let controller = 'bankUsers';
            //let redirectURL = site_url + 'admin/admin/fincare-admin';
            if (usertype == "login") {
                controller = 'login';
                redirectURL = site_url + 'home';

            }
          //  kendo.ui.progress($("body"), true);
            // Login request    
            $.ajax({
                url: site_url + controller + '/validateOTP',
                dataType: "json",
                type: 'post',
                data: { data : encryptData(loginParam) },
                success: function (response) {
                    // kendo.ui.progress($("body"), false);
                    if (response[0].code !=100) {
                        showErrorMsg('Please enter the correct OTP', form);
                    } else {
						// showSuccessMsg(response[0].status, form);
						//$('body').html(`<div clas<div class="loader"></div>`)	
                        //kendo.ui.progress($("body"), true);
                        // Redirect to dashboard

                        window.location = redirectURL;

                    }

                },
                error: function(xhr, status, error) {
                    if(xhr.status == 403){
                        window.location.href = site_url+"welcome/error/403";
                    }else{
                    showErrorMsg(messages.ajaxerror.en, form);
                    }
                }
            });

        }

    });
    // Super login otp
    $("#otpFormSuper").validate({

        normalizer: function (value) {
            return $.trim(value);
        },
        rules: {
            otpValue: {
                required: true
            }
        },
        messages: {
            otpValue: {
                required: "Please enter valid OTP"
            }
        }
        ,
        submitHandler: function (form) {

            let otpValue = $("#otpValue").val();
            let usertype = $("#usertype").val();

            // login data
            let loginParam = {
                'otpValue': otpValue
            }

            let controller = 'SuperUsers';
            var redirectURL = site_url + 'super/super/manage-bank';
            
            kendo.ui.progress($("body"), true);
            // Login request    
            $.ajax({
                url: site_url + controller + '/validateOTP',
                dataType: "json",
                type: 'post',
                data: { data : encryptData(loginParam) },
                success: function (response) {
                    //alert(response);
                   console.log(response);
                     kendo.ui.progress($("body"), false);
                    if (response[0].code !=100) {
                        showErrorMsg('Please enter the correct OTP', form);
                    } else {
                        // showSuccessMsg(response[0].status, form);
                        // $('body').html(`<div clas<div class="loader"></div>`) 
                        // kendo.ui.progress($("body"), true);
                        // Redirect to dashboard
                        console.log(redirectURL);
                        window.location = redirectURL;

                    }

                },
                error: function(xhr, status, error) {
                    //alert(xh.status);
                    if(xhr.status == 403){
                        window.location.href = site_url+"welcome/error/403";
                    }else{
                    showErrorMsg(messages.ajaxerror.en, form);
                    }
                }
            });

        }

    });
	$("#forgotPwForm").validate({

		normalizer: function (value) {
			return $.trim(value);
		},
		rules: {
			useridText: {
				required: true
			},
			corpidText: {
				required: function () {
					if ($("#usertype").val() == "Corporate")
						return true;
					else
						false;
				}
			}
		},
		messages: {
			useridText: {
				required: "Please enter valid user ID"
			},
			corpidText: {
				required: "Please enter valid corporate ID"
			}
		}
		,
		submitHandler: function (form) {
			showErrorMsg('Checking...', form);
			let usertype = $("#usertype").val();
			let useridText = $("#useridText").val();
			let corpidText = (usertype == "Corporate") ? $("#corpidText").val() : "1";
            //alert(usertype);
			let forgotpwParam = {
				'useridText': useridText,
				'corpidText': corpidText
			}

			let controller = 'bankUsers';
			if (usertype == "Corporate")
				controller = 'corporateUsers';
           // kendo.ui.progress(form, true);
			$.ajax({
				url: site_url + controller + '/forgotPassword',
				dataType: "json",
				type: 'post',
				data: { data : encryptData(forgotpwParam) } ,
				success: function (response) {
					 console.log(response);
                    //kendo.ui.progress(form, false);
					if (response[0].code ===100){
						$(".forgot_pw_module").hide();
						timerCountdown(".forgot_pw_otp_module");
						   $(".forgot_pw_otp_module").show();
						showSuccessMsg(response[0].status, form);
					} else {
						console.log(response[0].code === 0)
						showErrorMsg(response[0].status, form);
						console.log(response);
					}

				},
				error: function(xhr, status, error) {
					if(xhr.status == 403){
						window.location.href = site_url+"welcome/error/403";
					}else{
						showErrorMsg(messages.ajaxerror.en, form);
					}
				}
			});

		}

	})

	$("#forgotPwOtpForm").validate({

		normalizer: function (value) {
			return $.trim(value);
		},
		rules: {
			otpValue: {
				required: true
			}
		},
		messages: {
			otpValue: {
				required: "Please enter valid OTP"
			}
		}
		,
		submitHandler: function (form) {

			let otpValue = $("#forgotOtpValue").val();
			let usertype = $("#usertype").val();

			// login data
			let loginParam = {
				'otpValue': otpValue
			}

			let controller = 'bankUsers';
			if (usertype == "Corporate") {
				controller = 'corporateUsers';
			}
            kendo.ui.progress($("#forgotPwOtpForm"), true);
			// Login request    
			$.ajax({
				url: site_url + controller + '/forgotPasswordValidateOTP',
				dataType: "json",
				type: 'post',
				data: { data : encryptData(loginParam) },
				success: function (response) {
                    kendo.ui.progress($("#forgotPwOtpForm"), false);
					console.log(response);
					if (response.code !== 100) {
						showErrorMsg('Please enter the correct OTP', form);
					} else {
                        $(".forgot_pw_otp_module").hide();
                        showErrorMsg("Please reset your password", $("#resetPwForm"));
                        $(".reset_pw_module").show();
					}
				},
				error: function(xhr, status, error) {
					if(xhr.status == 403){
						window.location.href = site_url+"welcome/error/403";
					}else{
					showErrorMsg(messages.ajaxerror.en, form);
					}
				}
			});

		}

	});

    // Reset Password
    $("#resetPwForm").validate({

        normalizer: function (value) {
            return $.trim(value);
        },
        rules: {
            npwd: {
				required: true,
				minlength: 8,
				passwordCheck: true
            },
            cpwd: {
				required: true,
				equalTo: "#npwd"  
            }
        },
        messages: {
            npwd: {
				required: "Please enter your new password",
                minlength: "Password must be at least 8 characters long"
            },
            cpwd: {
				required: "Please confirm your new password",
				equalTo: "Please make sure the passwords match"
            }
        }
        ,
        submitHandler: function (form) {
            let npwd = $("#npwd").val();
            let cpwd = $("#cpwd").val();
            let usertype = $("#usertype").val();

            // login data
            let loginParam = {
                'npwd': npwd,
                'cpwd': cpwd
            }

            let controller = 'bankUsers';
            if (usertype == "Corporate")
                controller = 'corporateUsers';

            kendo.ui.progress($("#resetPwForm"), true);
            // Login request    
            $.ajax({
                url: site_url + controller + '/resetPassword',
                dataType: "json",
                type: 'post',
                data: { data : encryptData(loginParam) } ,
                success: function (response) {
					console.log(response);
                    kendo.ui.progress($("#resetPwForm"), false);
					if (response[0].code ===100){
						console.log(response);
						showSuccessMsg(response[0].status, form);
						setTimeout(()=> window.location = site_url + controller + '/login', 1000) 
					} else {
						showErrorMsg(response[0].status, form);
						console.log(response);
					}
                    // if (response[0].code != 0) {
                    //     //showErrorMsg(response[0].status,form);  
                    // } else {
                    //     //showSuccessMsg(response[0].status,form); 
                    //     // Redirect to login page
                    //     window.location = site_url + controller + '/login';

                    // }


                },
                error: function(xhr, status, error) {
                    if(xhr.status == 403){
                        window.location.href = site_url+"welcome/error/403";
                    }else{
                    showErrorMsg(messages.ajaxerror.en, form);
                    }
                }
            });

        }

    });


    // Resend OTP  
    $(".resend_otp").on('click', function (e) {
        
        let form = $(this).parents('form');
        var form_id = $(this).parents('form').attr('id');;
        alert(form_id);
        console.log("form: "+form_id);
        let usertype = $("#usertype").val();

        let otpttempt = $("#otpattempt").val();
        let logintype = $("#logintype").val();

        let forgotAttempt = $("#forgotattempt").val();        
        let forgottype = $("#forgottype").val();

        let bankAttempt = $("#Bankotpattempt").val();        
        let banktype = $("#Banklogintype").val();

        let otpAttemt = $("#otpAttempt").val();
        let controller = 'bankUsers';
        if (usertype == "login")
            controller = 'login';

       // kendo.ui.progress($(".resend_otp"), true);
        if(usertype == 'login')
        {


            if(form_id =='otpForm' )
            {
                //alert("otp: "+form_id);
                var otpParams = {
                    'otpattempt':otpttempt,                
                    'type':logintype,                
                    
                }
            }
            // if(form_id =='forgotPwOtpForm')
            // {
            //     //alert(form_id);
            //     var otpParams = {               
            //         'otpattempt':forgotAttempt,                
            //         'type':forgottype,                
            //     }
            // }

        }
        if(usertype == 'Bank')
        {
            
        
            if(form_id =='otpForm' )
            {
                //alert("otp: "+form_id);
                var otpParams = {
                    'otpattempt':bankAttempt,                
                    'type':banktype,                
                    
                }
            }
            // if(form_id =='forgotPwOtpForm')
            // {
            //     //alert(form_id);
            //     var otpParams = {               
            //         'otpattempt':forgotAttempt,                
            //         'type':forgottype,                
            //     }
            // }

        }
         $("#show_loader").show();
        $.ajax({
            url: site_url + controller + '/resendLoginOTP',
            dataType: "json",
            type: 'post',
            data: { data : encryptData(otpParams) } ,
            success: function (response) {
                 $("#show_loader").hide();
                if (response[0].code == "-1") {
                     //kendo.ui.progress($(".resend_otp"), false);
                    showErrorMsg('Please enter the correct OTP', form);
                }
                else if (response[0].code =="-2") {
                    // kendo.ui.progress($(".resend_otp"), false);
                    showErrorMsg(response[0].status, form);
                } else {
                    showSuccessMsg(response[0].status, form);
                    //kendo.ui.progress($(".resend_otp"), false);
                    $(".otp_form #resendMSG").removeClass("d-none");
                    $(".otp_form #resendBtn").addClass("d-none");
                    timerCountdown(".otp_form");
                }


            },
            error: function(xhr, status, error) {
                if(xhr.status == 403){
                    window.location.href = site_url+"welcome/error/403";
                }else{
                showErrorMsg(messages.ajaxerror.en, form);
                }
            }
        });

    });



    // Re-Generate Captcha
    $(".generate_captcha").on('click', function () {

		$("#captchaCode").val('');
        let form = $(this).parents('form');
        let usertype = $("#usertype").val();
        let controller = 'bankUsers';
        if (usertype == "Corporate")
            controller = 'corporateUsers';

        $.ajax({
            url: site_url + controller + '/regenerateCaptcha',
            dataType: "html",
            type: 'post',
            success: function (response) {
				$(".captcha_image").html(response);

            },
            error: function(xhr, status, error) {
                if(xhr.status == 403){
                    window.location.href = site_url+"welcome/error/403";
                }else{
                    showErrorMsg('Captcha Generation Failed', form);
                }
            }
        });

    });


    // Get Mini Statement
    $(".widget2_miniStmt").on('click', function () {

        let accountNumber = $(".widget2_accountNo").find(":selected").val();
        let fromDate=$("#widget2_fromDate").val();
        let toDate=$("#widget2_toDate").val();
        
        // kendo.ui.progress($(".modal-body"), true);
       

    });


    // Get Account Statement
    $(".widget2_acctStmt").on('click', function () {

        let value = $( 'input[name=ministatem]:checked' ).val();
        alert(value);
        let accountNumber = $(".widget2_accountNo").find(":selected").val();
        let fromDate = $("#widget2_fromDate").val();
        let toDate = $("#widget2_toDate").val();
        globalClickDiv = "#demo1";
		// $(".widget2_acctCurrentBal").text
       if(value=='ministate')
      // $('.from_to_date').prop("disabled" ,true);
      // $(".testmini").attr('disabled','true');
        getMiniStatement(accountNumber,fromDate,toDate,value,"#ministatement");
        if(value=='statmebydate')
        
       getAccountStatement(accountNumber, fromDate, toDate,"#acct_statment");
    });


    // Get Balance Widget Account & Mini Statement
    $(".widget1_balanceMore").on('click', function () {

        let accountNumber = $(".widget1_accountNo").find(":selected").val();
        //let fromDate=$("#widget2_fromDate").val();


        var currentDate = new Date(currentServerDate);
        
		let toDate = formatDate(currentDate);
        let fromDate = formatDate(currentDate.setDate(currentDate.getDate() - 30));
  
		$(".widget2_acctCurrentBal").text($(".widget1_availBalance").eq(0).text());
        // let fromDate = '10/10/2011';
        // let toDate = '10/10/2019';
        
        getMiniStatement(accountNumber,fromDate,toDate,"#bal_clearing");
        getAccountStatement(accountNumber, fromDate, toDate, "#statement_grid");

    });





    
    // Get Mini Statement Function
    function getMiniStatement(accountNumber,fromDate,toDate,value,ID) {

       // form = $("#add-corporate");
        let miniStatementParam = {
            'accountNumber': accountNumber,
            'fromDate': fromDate,
            'toDate': toDate,
            'value':value
        }
        kendo.ui.progress($("body"), true);

        $.ajax({
            url: site_url + 'dashboard/get_mini_statement',
            dataType: "json",
            type: 'post',
            data: { data : encryptData(miniStatementParam) },
            success: function (response) {
                console.log(response);
                 kendo.ui.progress($("body"), false);
                //kendo.ui.progress($(".modal-body"), false);

                if(checkDeleteUser(response)){
                    return false;
                }
                $("#ministatement").modal("show");
                $(".widget2_miniAccountNo").text(accountNumber);
                //$(".widget2_miniFromDate").text(fromDate);                           
                //$(".widget2_miniToDate").text(toDate);

                let tr = '';
                let openingBalance = 0;
                let currentBalance = 0;
                let totalCredited = 0;
                let totalDebited = 0;
                if (response != "") {
                
                    var result = [];
                    for (var i = 0; i < response.length; i++) {

                        if (i == 0) {
                           // currentBalance = parseFloat(response[i]['ns26EndingBalance']).toFixed(2);
                            openingBalance = formatToInr(response[i]['ns26OpeningBalance']);
                        }
                        if (response[i]['ns26DebitCredit'] == 0) {
                            totalDebited += parseFloat(response[i]['ns26TranAmount']);
                        } else if (response[i]['ns26DebitCredit'] == 1) {
                            totalCredited += parseFloat(response[i]['ns26TranAmount']);
						}
                        var ns26TranAmount = formatToInr(response[i]['ns26TranAmount']);
                        var row = [];
                        // row['srno'] = (i + 1);
                        row['date'] = formatDate(response[i]['ns26TranDate']);
                        row['description'] = response[i]['ns26TranComment'].length > 0 ? response[i]['ns26TranComment'] : '-';
                        row['balance'] = formatToInr(response[i]['ns26EndingBalance']);
                        row['credit'] = (response[i]['ns26DebitCredit'] == 1 ? ns26TranAmount : "-");
                        row['debit'] = (response[i]['ns26DebitCredit'] == 0 ? ns26TranAmount : "-");
                        result[i] = row;
                        currentBalance = formatToInr(response[i]['ns26EndingBalance']);
                    }
					totalCredited = formatToInr(totalCredited);
					totalDebited = formatToInr(totalDebited);
                    $(ID+" .grid").data('kendoGrid').dataSource.data(result);

                } else {
                   
                    tr += '<tr>';
                    tr += '<td colspan="6" align="center">No Records Found</td>';
                    tr += '</tr>';
                    $(".widget2_ministmtData").html(tr);
                }

                //
                $(".widget2_miniOpeningBal").text(openingBalance);
                $(".widget2_miniCurrentBal").text(currentBalance);
                $(".widget2_miniTotalCredit").text(totalCredited);
                $(".widget2_miniTotalDebit").text(totalDebited);
              
            },
            error: function(xhr, status, error) {
                if(xhr.status == 403){
                    window.location.href = site_url+"welcome/error/403";
                }else{
             //   showErrorMsg(messages.ajaxerror.en, form);
                }
            }
        });

    }
    // $('#Accstatement').on('hidden.bs.modal', function (e) {
    //             console.log("Modal hidden");
    //             // alert('Hi');
    //              $("#Accstatement #acct_statment").html("");
    //              //$("#Accstatement #widget2_acctstmtData").html("");
    //             // $("#bootstrap-confirm-dialog .container2").html("");
    //             // $('#bootstrap-confirm-dialog .modal-title').html('Are you sure to proceed with the following information?');   
    //             // $('#next-otp-screen-preclouser').show();
    //             //$('#acct_statment').show();

    //             // $('#otp-screen').hide();
    //             // $("#forcloseDIV").show();
    //          });

    // Get Account Statement Function
    function getAccountStatement(accountNumber, fromDate, toDate,ID) {

//alert(fromDate);alert(toDate);
        let acctStatementParam = {
            'accountNumber': accountNumber,
            'fromDate': fromDate,
            'toDate': toDate,
            
        }
     
        kendo.ui.progress($("body"), true);
        $.ajax({
            url: site_url + 'dashboard/get_account_statement',
            dataType: "json",
            type: 'post',
            data: { data : encryptData(acctStatementParam) },
            success: function (response) {
                //console.log("account"+response[1]['ns41EndingBalance']);
                kendo.ui.progress($("body"), false);
               

                if(checkDeleteUser(response)){
                    return false;
                }

                $("#Accstatement").modal("show");
                $(".widget2_acctAccountNo").text(accountNumber);
                $(".widget2_acctFromDate").text(fromDate);
                $(".widget2_acctToDate").text(toDate);
                let tr = '';
                let openingBalance = 0;
                let currentBalance = 0;
                let totalCredited = 0;
                let totalDebited = 0;
                let counter = 0;
                if (response != "") {

					var result = [];
                    for (var i = 0; i < response.length; i++) {

						if (response[i]['ns41Description'] == "Opening Balance") {
                            console.log()
							openingBalance = formatToInr(response[i]['ns41EndingBalance']);
							continue;
                        } 
                        if (response[i]['ns41Description'] == "Closing Balance") {
							currentBalance = formatToInr(response[i]['ns41EndingBalance']);
							continue
						}
                        // create Start Syam
                        // if(response[0]['ns41DebitCredit'] == 'D')
                        // {   
                        //        totalDebited +=     parseFloat(response[0]['ns41EndingBalance']) + parseFloat(response[0]['ns41Amount'])

                        //     //totalDebited += parseFloat(response[i]['ns41Amount']);
                        // }
                        // if(response[0]['ns41DebitCredit'] == 'C')
                        // {
                        //      totalCredited +=     parseFloat(response[0]['ns41EndingBalance']) - parseFloat(response[0]['ns41Amount'])
                        //     //totalCredited += parseFloat(response[i]['ns41Amount']);
                        // }
                        // Create End Syam

                        if (response[i]['ns41DebitCredit'] == 'D') {
                            totalDebited += parseFloat(response[i]['ns41Amount']);
                        } else if (response[i]['ns41DebitCredit'] == 'C') {
                            totalCredited += parseFloat(response[i]['ns41Amount']);
						}
						
                        var ns41Amount = formatToInr(response[i]['ns41Amount']);
                        var row = [];
                        // row['srno'] = i;
                        row['date'] = formatDate(response[i]['ns41TranDate']);
                        row['description'] = response[i]['ns41Description'].length > 0 ? response[i]['ns41Description'] : '-';
                        row['balance'] = formatToInr(response[i]['ns41EndingBalance']);
                        row['credit'] = (response[i]['ns41DebitCredit'] == 'C' ? ns41Amount : "-");
                        row['debit'] = (response[i]['ns41DebitCredit'] == 'D' ? ns41Amount : "-");
						result[i-1] = row;
                    }

                    console.log(result[0]);
                    if(result[1]['debit'] !="-")
                    {
                        var cal=parseFloat(response[0]['ns41EndingBalance']) + parseFloat(result[1]['debit']);
                        console.log('balance:'+response[0]['ns41EndingBalance']);
                        console.log('debit:'+result[0]['debit']);
                        console.log('credit:'+cal);
                    }
                    // result[0]['debit']=result[0]['debit'];
                    // result[0]['balance']=cal;
					totalCredited = formatToInr(totalCredited);
                    totalDebited = formatToInr(totalDebited);
                    $(ID).data('kendoGrid').dataSource.data();
                    $(ID).data('kendoGrid').refresh();
                    $(ID).data('kendoGrid').dataSource.data(result);
                    $(ID).data('kendoGrid').resize();

                } else {

     //                tr += '<tr>';
     //                tr += '<td colspan="6" align="center">No Records Found</td>';
					// tr += '</tr>';
					//$(".widget2_acctstmtData").html(tr);
                    $(ID).data('kendoGrid').dataSource.data(response);
                }
                $(".widget2_acctOpeningBal").text(openingBalance);
                $(".widget2_acctClosingBal").text(currentBalance);
                $(".widget2_acctTotalCredit").text(totalCredited);
                $(".widget2_acctTotalDebit").text(totalDebited);
               // $(".widget2_FDstatus").text(response.FDStatus);

            },
            error: function(xhr, status, error) {
                if(xhr.status == 403){
                    window.location.href = site_url+"welcome/error/403";
                }else{
                showErrorMsg(messages.ajaxerror.en);
                }
            }
        });

    }



    // Get FD Detail Statement
    $(".widget3_detailStatement").on('click', function () {

		let accountNumber = $(".widget3_accountNo").find(":selected").val();
		// let currentDate = new Date();
		// let toDate = formatDate(currentDate);
        // let fromDate = formatDate(currentDate.setDate(currentDate.getDate() - 30));
        let fromDate = $("#demo2 #widget2_fromDate").val();
        let toDate = $("#demo2 #widget2_toDate").val();
		$(".widget2_acctCurrentBal").text( $(".widget3_availBalance").text());
        // let fromDate = '10/10/2011';
        // let toDate = '10/10/2019';
        globalClickDiv = "#demo2";
        //kendo.ui.progress($(".modal-body"), true);
        // getMiniStatement(accountNumber, fromDate, toDate, "#bal_clearing");
        getAccountStatement(accountNumber, fromDate, toDate, "#acct_statment");

    });




    // Bulk Upload Function
    $("#bulkFileForm").validate({

        normalizer: function (value) {
            return $.trim(value);
        },
        rules: {
            files: {
                required: true
            }
        },
        messages: {
            files: {
                required: "Please attach valid file"
            }
        },
        submitHandler: function (form) {

            let formData = new FormData(form);
            kendo.ui.progress($("body"), true);
            $.ajax({
                url: site_url + 'api/services/bulk_upload',
                dataType: "json",
                type: 'post',
                cache: false,
                contentType: false,
                processData: false,
                data: { data : encryptData(formData) },
                success: function (response) {

                    kendo.ui.progress($("body"), false);
                    if (response[0].code != 0) {
                        showErrorMsg(response[0].status, form);
                    } else {
                        showSuccessMsg(response[0].status, form);
                        $("#bulk-data").html(response[0].html);
                        //location.reload();
                    }

                },
                error: function(xhr, status, error) {
                    if(xhr.status == 403){
                        window.location.href = site_url+"welcome/error/403";
                    }else{
                    showErrorMsg(messages.ajaxerror.en, form);
                    }
                }
            });

        }

    });




    /**********  GD End  **********/


	/**********  Adhir Start  **********/
	
	// Password check validation
	$.validator.addMethod("passwordCheck", function(value, element) {
		if (this.optional(element)) {
			return true;
		// } else if (!/[A-z]/.test(value)) {
		// 	return false;
		} else if (!/[A-Z]/.test(value)) {
			return false;
		} else if (!/[!@ #$%^&*]+/.test(value)) {
			return false;
		// } else if (!/[0-9]/.test(value)) {
		// 	return false;
		}

		return true;
	},
	"Password must meet the criteria");

	$.validator.addMethod( "payeeName", function( value, element ) {
		return this.optional( element ) || /^[\w\s]+$/i.test( value );
	}, "Please enter valid name" );
    $.validator.addMethod('minStrict', function (value, element) {
        return   /(^[1-9][0-9]{0,10}|100000000000000)$/i.test( value );
    },"Please  enter correct Amount");	
 //add by pavan
    $("#add-corporate").validate({
        ignore: [],
        normalizer: function (value) {
            return $.trim(value);
        },
        rules: {
            corp_cifid: {
                required: true,
                digits: true
            },
            
            // corpname: {
            //     required: true
            // },
            // mobile: {
            //     required: true
            // },
            // emailId: {
            //     required: true
            // },
            
            acctlimit: {
                required: true,
                // mobileNo: true,
                digits: true,
                minStrict:true,
                number: true
            },
           
            
        },
        messages: {
            corp_cifid: {
                required: "Please enter the corporate ID"
            },
            // corpname: {
            //     required: "Please enter a corporate name"
            // },
            // mobile: {
            //     required: "Please enter a mobile number"
            // },
            // emailId: {
            //     required: "Please enter a email id"
            // },
            acctlimit:{
               required:"Please enter  account limit"
           },
          
        },
        
        submitHandler: function (form) {
            var corporateLookup = $('#corporateLookup').val();
            //alert(corporateLookup);
            clearMsg(form);
            if(corporateLookup == 'true'){
                //let user_cifid = $('#user_cifid').val();
                let corp_cifid = $('#corp_cifid').val();
                let corp_name = $('#corpname').val();
                let acct_limit = $('#acctlimits').val();
                let mobile = $('#mobile').val();
                let emailId = $('#emailId').val();
                //let password = $('#npwd').val();
                //let mobileNo = $('#mobileNo').val();
                // kendo.ui.progress(form, true);
                $("#submit_corporate").prop("disabled",true);
                showErrorMsg("Processing...", form);
                    kendo.ui.progress($("body"), true);
                $.ajax({
                    type: 'POST',
                
                    url: site_url + 'admin/Admin/add_newcorporate',
                    data: { data : encryptData({'corp_cifid':corp_cifid,'corp_name':corp_name,'acct_limit':acct_limit,'mobile':mobile,'emailId':emailId}) },
                    dataType: 'json',
                    success: function (response) {
                        kendo.ui.progress($("body"), false);
                        showSucMsg('msg',"Corporate Added Successfully"); 
                        setTimeout(function () {
                            window.location.replace(site_url + "admin/admin/manage-group-limits");
                        }, 5000);

                    },
                    error: function(xhr, status, error) {
                        if(xhr.status == 403){
                            window.location.href = site_url+"welcome/error/403";
                        }else{
                            showErrorMsg(messages.ajaxerror.en,form);
                        }
                    }
                });  
            } else{
                showErrorMsg("Please Lookup First", form);
            }         
        }
    });

    $("#add-user-form").validate({
        ignore: [],
        normalizer: function (value) {
            return $.trim(value);
        },
        rules: {
            corp_cifid: {
                required: true,
                digits: true
            },
            user_cifid: {
                required: true,
                digits: true
            },
            name: {
                required: true
            },
            npwd: {
                required: function () {
                    if ($("#set_user_id").val()) {
                        return false
                    } else {
                        return true
                    }
                },
				minlength: 8,
				passwordCheck: true
            },
            mobileNo: {
                required: true,
                // mobileNo: true,
				digits: true,
				minlength: 10,
				maxlength: 10
            },
            emailId: {
                required: true,
                email: true
            },
            role: {
                role: true
            },
            group_select: {
                
                group_select: groupSelectFieldReqORNot("set_corp_id")
            }
        },
        messages: {
            corp_cifid: {
                required: "Please enter the corporate ID"
            },
            user_cifid: {
                required: "Please enter the user ID"
            },
            name: {
                required: "Please enter a name"
            },
            npwd: {
                required: "Please enter a password",
                minlength: "Password must be at least 8 characters long"
            },
            mobileNo: {
                required: "Please enter a mobile number",
                digits: "Make sure the mobile number is valid (numbers only)",
                minlength: "Make sure the mobile number is valid (10 digits)",
                maxlength: "Make sure the mobile number is valid (10 digits)"
            },
            emailId: {
                required: "Please enter an email ID",
                email: "Please enter a valid email"
            }
        },
        submitHandler: function (form) {
            clearMsg(form);
            let action = $('#action').val();
            let user_cifid = $('#user_cifid').val();
            let corp_cifid = $('#corp_cifid').val();
            let name = $('#name').val();
            let password = $('#npwd').val();
            let mobileNo = $('#mobileNo').val();
            let emailId = $('#emailId').val();
            let employeeNo = $('#employeeNo').val();
            let role = $("#role").data("kendoDropDownTree").value();
            let group = $("#group_select").data("kendoDropDownList").value();
            let corp_name = $('#corp_name').val();
            

            let lookup_user_id = $('#lookup_user_id').val();
            let lookup_corp_id = $('#lookup_corp_id').val();

            let userIdSet = $('#set_user_id').val();

            if (!userIdSet) {
                if (lookup_corp_id !== corp_cifid || lookup_user_id !== user_cifid) {
                    showErrorMsg('Please look up the Corporate and User CIF IDs before submitting', form);
                    return
                }
            }
            let addUserData = { 'action':action, 'corp_cifid':corp_cifid,'corp_name':corp_name,'user_cifid':user_cifid,'password':password,'name':name,'mobileNo':mobileNo,'emailId':emailId,'employeeNo':employeeNo,'role':role,'group':group}

			showErrorMsg("Processing...", form);
			kendo.ui.progress($("body"), true);
            $.ajax({
                type: 'POST',
                // beforeSend: function () {
				// 	kendo.ui.progress(form, true);
                //     // $('body').prepend("<div class='mainLoader'><div class='loader'></div></div>");
                // },
                url: site_url + 'admin/Admin/new_corp_user',
                data: { data : encryptData(addUserData) },
                dataType: 'json',
				success: function (response) {
                    kendo.ui.progress($("body"), false);
					if (response.code === "A"){
						showSuccessMsg(response.status, form);
						setTimeout(function () {
							window.location.replace(site_url + "admin/Admin/manage-corporate-user/");
						}, 5000);
					} else if (response.code === "U") {
						showSuccessMsg(response.status, form);
						setTimeout(function () {
							window.location.replace(site_url + "admin/Admin/manage-corporate-user/");
						}, 5000);
					} 
				}, 
				error: function(xhr, status, error) {
					if(xhr.status == 403){
						window.location.href = site_url+"welcome/error/403";
					}else{
						kendo.ui.progress($("body"), false);
						showErrorMsg('Error with adding groups', form);
					}
				}
			});            
        }
    });
// Look up User and Corporate CIF
$('#lookup_btn').on('click',function(){
	let form = $("#add-user-form");
	corp_cifid=$('#corp_cifid').val();
	user_cifid=$('#user_cifid').val();
	clearMsg(form);
    
	if (corp_cifid != "" && user_cifid != "") {
        showErrorMsg("Looking up user", form)
        kendo.ui.progress($("body"), true);
		$.ajax({
			type:'POST',
			url: site_url+'admin/Admin/lookup_call',
			data: { data : encryptData({'user_cifid':user_cifid, 'corp_cifid':corp_cifid}) },
			dataType:'json',
			success: function(result){
                kendo.ui.progress($("body"), false);
                console.log(result[0].ns13RtnCd);
                if (result[0].ns13RtnCd != 0){
                    showErrorMsg(result[0].status, form);
                }else{
                    $("#submit_user_btn").prop("disabled",false);
                    name = result[0].ns13Name;
                    mobileNo = result[0].ns13MobileNumber;
                    emailId = result[0].ns13Email;

                    employeeNo = user_cifid;

                    if (result[1].length > 0) {
                        $("#group_select").data("kendoDropDownList").setDataSource(result[1]);
                    }
                
                    $('#nameModal').text(name);
                    $('#emailIdModal').text(emailId);
                    $('#mobileNoModal').text(mobileNo);

                    $('#name').val(name);
                    $('#emailId').val(emailId);
                    $('#mobileNo').val(mobileNo);
                    $('#employeeNo').val(employeeNo);

                    $('#lookup_user_id').val(user_cifid);
                    $('#lookup_corp_id').val(corp_cifid);
                    $('#lookup_modal').modal('show');
                    clearMsg(form);
                }



				// if (result[0].code){
				// 	    showErrorMsg(result[0].status, form);
                //     form.trigger("reset");
                //     $("#submit_user_btn").prop("disabled",true);
				// } else if (result[0].ns13RtnCd === "100") {
				// 	showErrorMsg("No Record Found for Corporate ID: "+corp_cifid, form);
                //     form.trigger("reset");
                //     $("#submit_user_btn").prop("disabled",true);
				// } else if (result[1].ns13RtnCd === "100") {
				// 	showErrorMsg("No Record Found for User ID: "+user_cifid, form);
                //     form.trigger("reset");
                //     $("#submit_user_btn").prop("disabled",true);
				// } else if (result[0].ns13RtnCd !== "0" || result[1].ns13RtnCd !== "0") {
				// 	showErrorMsg("There was an error during lookup", form);
                //     form.trigger("reset");
                //     $("#submit_user_btn").prop("disabled",true);
				// } else if (!(result[1].ns13Name && result[1].ns13MobileNumber && result[1].ns13Email && result[0].ns13Name)) {
				// 	showErrorMsg("The CIF lookup did not provide all the required data", form);
                //     form.trigger("reset");
                //     $("#submit_user_btn").prop("disabled",true);
				// } else {
				// 	$("#submit_user_btn").prop("disabled",false);
				// 	name = result[1].ns13Name;
				// 	mobileNo = result[1].ns13MobileNumber;
				// 	emailId = result[1].ns13Email;
				// 	employeeNo = user_cifid;
				// 	corp_name = result[0].ns13Name;

				// 	if (result[2].length > 0) {
				// 		$("#group_select").data("kendoDropDownList").setDataSource(result[2]);
				// 	}
				
                //     $('#nameModal').text(name);
                //     $('#emailIdModal').text(emailId);
                //     $('#mobileNoModal').text(mobileNo);

                //     $('#name').val(name);
                //     $('#emailId').val(emailId);
                //     $('#mobileNo').val(mobileNo);
                //     $('#employeeNo').val(employeeNo);
                //     $('#corp_name').val(corp_name);

                //     $('#lookup_user_id').val(user_cifid);
                //     $('#lookup_corp_id').val(corp_cifid);
                //     $('#lookup_modal').modal('show');
                //     clearMsg(form);
                // }
			}
			, error: function(xhr, status, error) {
				console.log(error);
                if(xhr.status == 403){
                    window.location.href = site_url+"welcome/error/403";
                }else{
					kendo.ui.progress($("body"), false);
                    showErrorMsg('Error with look up',form);
                }
				}
		});
		
	} else {
		console.log('Error message');
		showErrorMsg('Please enter Corporate CIF ID and User ID',form);
	}
})
$('#lookupcorporate_call').on('click',function(){
	let form = $("#add-corporate");
	var corp_cifid=$('#corp_cifid').val();
	//user_cifid=$('#user_cifid').val();
	clearMsg(form);
    
	if (corp_cifid != "" ) {
        showErrorMsg("Looking up user", form)
        $("#submit_corporate").prop("disabled",true);
        kendo.ui.progress($("body"), true);
		$.ajax({
			type:'POST',
			url: site_url+'admin/Admin/lookupcorporate_call',
			data: { data : encryptData({ 'corp_cifid':corp_cifid}) },
			dataType:'json',
			success: function(result){
                // alert(result)
                kendo.ui.progress($("body"), false);
                if (result[0].code == '-1'){
                    showErrorMsg(result[0]['status'], form);
                }else if(result[0].ns13RtnCd == '100'){
                    showErrorMsg("No Record Find For "+corp_cifid, form);
                }else{
                     $("#corporateLookup").val("true");
                    $("#corpname").prop("readonly",true);
                    
                    $("#submit_corporate").prop("disabled",false);
                    var  corp_name = result[0].ns13Name;
                    
                    var mobileNo = result[0].ns13MobileNumber;
                    var emailId = result[0].ns13Email;
                    $('#corpname').val(corp_name);
                    $('#mobile').val(mobileNo);
                    $('#emailId').val(emailId);
                    $('#corpname').focus();
                    $('#mobile').focus();
                    $('#emailId').focus();
                   // employeeNo = user_cifid;

                    
                    $('#nameModal').text(corp_name);
                    $('#mobileModal').text(mobileNo);
                    $('#emailModal').text(emailId);

                    //$('#name').val(name);
                   //// $('#emailId').val(emailId);
                   // $('#mobileNo').val(mobileNo);
                   //// $('#employeeNo').val(employeeNo);

                    //$('#lookup_user_id').val(user_cifid);
                    $('#lookup_corp_id').val(corp_cifid);
                    $('#lookup_modalcorporate').modal('show');
                    //clearMsg(form);
                
                    clearMsg(form);
                }


			}
			, error: function(xhr, status, error) {
				console.log(error);
                if(xhr.status == 403){
                    window.location.href = site_url+"welcome/error/403";
                }else{
					kendo.ui.progress($("body"), false);
                    showErrorMsg('Error with look up',form);
                }
				}
		});
		
	} else {
		console.log('Error message');
		showErrorMsg('Please enter Corporate CIF ID ',form);
	}
})
$("#admin_pw_change_form").validate({
	normalizer: function( value ) {
		return $.trim( value );
	},
	rules: {
		opwd: {
			required: true
		},
		npwd: {
			required: true,
            minlength: 8,
            passwordCheck: true
		},
		cpwd: {
			required: true,
			equalTo: '#npwd'
			}
		},            
	messages:{
		opwd: {
			required: "Please enter the old password"
		},
		npwd: {
			required: "Please enter the new password"
		},
		cpwd: {
			required: "Please confirm the new password",
			equalTo: "Please enter the same password as before"
		}                           
	},
	submitHandler: function(form) {
		
		let userId = $('#corp_user_id').val();
		let corpId = $('#corp_id').val();
		let opwd = $('#opwd').val()
		let npwd = $('#npwd').val()
		let cpwd = $('#cpwd').val()

		let changepwParam={
			'userId':userId,
			'corpId':corpId,
			'opwd':opwd,
			'npwd':npwd,
			'cpwd':cpwd
		}
	    kendo.ui.progress($("body"), true);
		$.ajax({
			url: site_url+'admin/Admin/adminChangePassword',
			dataType: "json",
			type:'post',
			data:{ data : encryptData(changepwParam) },
			success: function(response){
                 kendo.ui.progress($("body"), false);
				if(response[0].code!=0){
					showErrorMsg(response[0].status,form);
				}
				else{
					showSuccessMsg(response[0].status,form);
					setTimeout(function() {
						$('#reset_password_modal').modal('hide');
					}, 1000);
				}
			},
            error: function(xhr, status, error) {
                if(xhr.status == 403){
                    window.location.href = site_url+"welcome/error/403";
                }else{
                showErrorMsg(messages.ajaxerror.en,form);
                }
			}
		});
	}
});
	


	$('#npwd').keyup(function() {
		// keyup code here
		var pswd = $(this).val();
		if ( pswd.length < 8 ) {
			$('#length').removeClass('pswd_valid').addClass('pswd_invalid');
			$('#length').children('.pwicon').removeClass('k-i-check-circle').addClass('k-i-close-circle');
		} else {
			$('#length').removeClass('pswd_invalid').addClass('pswd_valid');
			$('#length').children('.pwicon').removeClass('k-i-close-circle').addClass('k-i-check-circle');
		}
		//validate letter
		// if ( pswd.match(/[A-z]/) ) {
		// 	$('#letter').removeClass('invalid').addClass('valid');
		// 	$('#letter').children('.pwicon').removeClass('k-i-close-circle').addClass('k-i-check-circle');
		// } else {
		// 	$('#letter').removeClass('valid').addClass('invalid');
		// 	$('#letter').children('.pwicon').removeClass('k-i-check-circle').addClass('k-i-close-circle');
		// }

		//validate capital letter
		if ( pswd.match(/[A-Z]/) ) {
			$('#capital').removeClass('pswd_invalid').addClass('pswd_valid');
			$('#capital').children('.pwicon').removeClass('k-i-close-circle').addClass('k-i-check-circle');
		} else {
			$('#capital').removeClass('pswd_valid').addClass('pswd_invalid');
			$('#capital').children('.pwicon').removeClass('k-i-check-circle').addClass('k-i-close-circle');
		}

		//validate number
		// if ( pswd.match(/\d/) ) {
		// 	$('#number').removeClass('pswd_invalid').addClass('pswd_valid');
		// 	$('#number').children('.pwicon').removeClass('k-i-close-circle').addClass('k-i-check-circle');
		// } else {
		// 	$('#number').removeClass('pswd_valid').addClass('pswd_invalid');
		// 	$('#number').children('.pwicon').removeClass('k-i-check-circle').addClass('k-i-close-circle');
		// }
		//validate special character
		if ( pswd.match(/[!@#$%^&*]+/) ) {
			$('#special').removeClass('pswd_invalid').addClass('pswd_valid');
			$('#special').children('.pwicon').removeClass('k-i-close-circle').addClass('k-i-check-circle');
		} else {
			$('#special').removeClass('pswd_valid').addClass('pswd_invalid');
			$('#special').children('.pwicon').removeClass('k-i-check-circle').addClass('k-i-close-circle');
		}
	}).focus(function() {
		$('#pswd_info').show();
	}).blur(function() {
		$('#pswd_info').hide();
	});

    /**********  Adhir End  **********/

    /**********  Neeraj Start  **********/

    $('.dropdown-container').each(function () {

        var cur_obj = $(this);
        var text = cur_obj.prev('a').text();
        var sb = $(this).find('.submenu');
        var submenu_count = $(this).find('.submenu').length;
        if (submenu_count == 0)
        {
            cur_obj.prev('a').hide();
        } else {
            cur_obj.prev('a').show();
        }
    })


    /**********  Neeraj End  **********/


	/**********  Ali Start  **********/
	//pavan code to upload excel sheet for holiday master
    $("#holidaylist").validate({

        rules: {
            files: {
                required: true
               // accept:"xls,xlsx"
            }
        },
        messages: {
            files: {
                required: "Please attach  file"
               // accept:"select only excel sheet"
            }
        },
        submitHandler: function (form) {

            let formData = new FormData(form);
           // alert(formData);
            $.ajax({
                url: site_url + 'super/super/holidaylist_upload',
                dataType: "json",
                type: 'post',
                cache: false,
                contentType: false,
                processData: false,
                data: { data : encryptData(formData) },
                success: function (response) {

                  if (response[0].code != 0) {
                        showErrorMsg(response[0].status, form);
                    } else {
                        showSuccessMsg(response[0].status, form);
                        $("#holiday-data").html(response[0].html);
                        //location.reload();
                    }

                },
                error: function(xhr, status, error) {
                    //alert(xhr.status);
                    if(xhr.status == 403){
                        window.location.href = site_url+"welcome/error/403";
                    }else{
                    showErrorMsg(messages.ajaxerror.en, form);
                    }
                }
            });

        }

    });
  // Dashboard Payee Form
  $.validator.addMethod('AccVal', function (value, element) {
       // return   /(^[\w\s]{0,16})$/i.test( value );
        return this.optional(element) || /^[a-zA-Z0-9]+$/.test( value );

    },"Please enter valid account no....."); 
     $.validator.addMethod('ConAccVal', function (value, element) {
       return this.optional(element) || /^[a-zA-Z0-9]+$/.test( value );
    },"Please confirm account no....");

    // Dashboard Payee Form
    $("#addPayeeForm").validate({
        normalizer: function (value) {
            return $.trim(value);
        },
        rules: {
            acc_name: {
                required: true,
                payeeName: true
            },
            acc_no: {
                required: true,
                AccVal:true,
                //digits: true,
                maxlength: 16,
            },
            conf_acc_no: {
                required: true,
                //digits: true,
                ConAccVal:true,
                maxlength: 16,
                equalTo: '#acc_no'
            },
            short_name: {
                required: true,
                payeeName: true,
                minlength: 4
            },
            bnf_bankIfscCode: {
                required: function (element) {
                    var transferType = $('select[name="beneficiaryType"] option:selected').val();
                    if (transferType == "1") {
                        return false;
                    } else {
                        return true;
                    }
                },
                minlength: 11,
                maxlength: 11
            },
        },
        messages: {
            acc_name: {
                required: "Please enter valid account name"
            },
            acc_no: {
                required: "Please enter valid account no",
                //digits: "Please enter valid account no",
                maxlength: "Account Number should be 16 digits"
            },
            conf_acc_no: {
                required: "Please confirm account no",
                //digits: "Please confirm account no",
                maxlength: "Account Number should be 16 digits",
                equalTo: "Account Number mismatch"
            },
            short_name: {
                required: "Please enter valid short name",
                maxlength: "Please enter 4 characters or more"
            },
            bnf_bankIfscCode: {
                required: "Please enter valid IFSC",
                minlength: "IFSC should be 11 characters",
                maxlength: "IFSC should be 11 characters"
            }
        },
        submitHandler: function (form) {
            let beneficiaryType = $("#beneficiaryType").val();
            let AccountType = $("#AccountType").val();
            let acc_name = $("#acc_name").val();
            let acc_no = $("#acc_no").val();
            let conf_acc_no = $("#conf_acc_no").val();
            let short_name = $("#short_name").val();
            let payee_narration = $("#payee_narration").val();
            let bnf_bankIfscCode = $("#bnf_bankIfscCode").val();
            let addPayeeParams = {
                "beneficiaryType" : beneficiaryType,
                "AccountType" : AccountType,
                "acc_name" : acc_name,
                "acc_no" : acc_no,
                "conf_acc_no" : conf_acc_no,
                "short_name" : short_name,
                "payee_narration" : payee_narration,
                "bnf_bankIfscCode" : bnf_bankIfscCode
            }
            // $("#payeeform").addClass("hide");
            // $("#addPayeeID").addClass("hide");
            // $("#payeeOTP").removeClass("hide");
            // $("#addPayeeverifyOTP").removeClass("hide");
                  // Add Payee Request    
            // let ifsc_code = 
            kendo.ui.progress($("body"), true);
                $.ajax({
                    url: site_url + 'dashboard/addPayee',
                    dataType: "json",
                    type: 'post',
                    data: { data : encryptData(addPayeeParams) },
                    success: function (response) {
                        kendo.ui.progress($("body"), false);
                        if(checkDeleteUser(response)){
                            return false;
                        }

                        if (response[0].code != 0) {
                            showErrorMsg(response[0].status, form);
                        } else {
                            alert(
                                "IFSC: "+response[0].data.ifsc_code+
                                "\nBank name: "+response[0].data.bank_name+
                                "\nBank branch: "+response[0].data.bank_branch
                                );
                            $(form).trigger("reset");
                            
                            showSuccessMsg(response[0].status, form);
                            setTimeout(() => clearMsg(form), 5000)
                            $("#addPayeeForm input").val("");
                            $("#addPayeeForm textarea").val("");
                            $("#addPayeeForm select").val("").trigger("change");
                            $("#addPayeeForm select").data("kendoDropDownList").value("");
                        }


                    },
                    error: function(xhr, status, error) {
                        if(xhr.status == 403){
                            window.location.href = site_url+"welcome/error/403";
                        }else{
                            showErrorMsg(messages.ajaxerror.en, form);
                        }
                    }
                });

        }

    });
    function onInitOpen(e) {
            kendoConsole.log("event :: initOpen");
        }

        function onOpen(e) {
            kendoConsole.log("event :: open");
        }

        function onShow(e) {
            kendoConsole.log("event :: show");
        }

        function onHide(e) {
            kendoConsole.log("event :: hide");
        }

        function onClose(e) {
            show.fadeIn();
            kendoConsole.log("event :: close");
        }
        function onCancel(e) {
            kendoConsole.log("action :: cancel");
        }

        function onOK(e) {
            kendoConsole.log("action :: OK");
        }

       
    /**********  Ali End  **********/


    /******************** Vicky Start **********************/


    $("#openDeposit").validate({

      
        submitHandler: function (form) 
        {
            let deposittype = $("#deposittype").val()||0;
            let accountno = $("#accountno").val()||0;
            let depositamount = $("#depositamount").val()||0;
            let deposittenuremonths = $("#deposittenuremonths").val()||0;
            let deposittenureyears = $("#deposittenureyears").val()||0;
            let deposittenuredays = $("#deposittenuredays").val()||0;
            let residenceoutside = $("#residenceoutside").val()||0;
            let maturityoption = $("#maturityoption").val()||0;
            //alert("deposityear"+deposittenureyears);
            //alert("depositmonths"+deposittenuremonths);
            //alert("depositdays"+deposittenuredays);
//alert($('#deposittenuremonths'.val()));
            // fd data
            let fdParam = {
                'deposittype': deposittype,
                'accountno': accountno,
                'depositamount': depositamount,
                'deposittenuremonths': deposittenuremonths,
                'deposittenureyears': deposittenureyears,
                'deposittenuredays': deposittenuredays,
                'residenceoutside': residenceoutside,
                'maturityoption' : maturityoption
            }
           
             console.log(fdParam);
             //pre_obj(fdParam);
            if (!depositMinMaxTermFlag) {
                //addError($("#deposittype").closest(".form-group"),"#fdtypeE","Select Proper FD Type",true);
            } else  if ( (deposittenuredays == 0 || deposittenuredays == "") && (deposittenuremonths == 'null' ||deposittenuremonths == 0 || deposittenuremonths == "") && (deposittenureyears == 0 || deposittenureyears == "")) {
                addError("#tenureFormE","#termtenureE",depositeMsg,true);
            } else 
            {
                addError($("#deposittype").closest(".form-group"),"#fdtypeE","",false)
                addError("#tenureFormE","#termtenureE",depositeMsg,false);                
                let yearday = deposittenureyears * 365;
                let monthday = deposittenuremonths * 30;


               // let days = Number(yearday) + Number(monthday) + Number(deposittenuredays);  
                let days = Number(yearday) + Number(monthday) + Number(deposittenuredays);//alert(days);
                 let maxterminmonth=$("#monthmaxterm").val();//alert(maxterminmonth+"totlamonths");
                 let maxterminyear=maxterminmonth/12;//alert(maxterminyear+"years");
                 let maxdaysperyear=maxterminyear*365;//alert(maxdaysperyear+"indays")
//alert($("#monthmaxterm").val());alert(maxdaysperyear+"calculateddays");
                let minTerm = (parseInt($("#monthminterm").val())*30) + parseInt($("#dayminterm").val());
                let maxTerm = (maxdaysperyear) + parseInt($("#daymaxterm").val());

                // let minTerm = (parseInt($("#monthminterm").val())*30) + parseInt($("#dayminterm").val());

                // let maxTerm = (parseInt($("#monthmaxterm").val())*30) + parseInt($("#daymaxterm").val()+50);
                let amount = parseInt($("#depositamount").val());

                if ((amount >= parseInt($("#minamount").val()) && (amount <= parseInt($("#maxamount").val())))) 
                {
                    if (days >= minTerm && days <= maxTerm) 
                    {
                        var tenure = "";
                        if (Number(deposittenureyears) != 0 ||Number(deposittenureyears) != 'null' ||  deposittenureyears != "") { if(Number(deposittenureyears) < 2)  tenure += deposittenureyears+" Year "; else tenure += deposittenureyears+" Years ";  }

                        if ( Number(deposittenuremonths) != 'null' ||Number(deposittenuremonths) != 0 || deposittenuremonths != "") {  if(Number(deposittenuremonths) < 2)  tenure += deposittenuremonths+" Month "; else tenure += deposittenuremonths+" Months ";  }
                        if ( Number(deposittenuredays) != 0 || deposittenuredays != "") {  if(Number(deposittenuredays) < 2)  tenure += deposittenuredays+" Day "; else tenure += deposittenuredays+" Days ";  }
                        
                        
                        var text = "<table><tr><td>Deposit Type  </td> <td> &nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td><td>"+$("#deposittype option:selected").text()+"</td></tr> " 
                                    +"<tr><td>Maturity Option </td><td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td><td>"+$("#maturityoption option:selected").text()+"</td></tr>"
                                    +"<tr><td>Debit Account Number </td><td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td><td>"+$("#accountno option:selected").text()+"</td></tr>"
                                    +"<tr><td>Amount of Deposit </td><td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td><td>"+formatToInr($("#depositamount").val())+"</td></tr>"
                                    +"<tr><td>Tenure of Deposit </td><td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td><td>"+tenure+"</td></tr>"
                                    +"</table>";

                        $("#bootstrap-confirm-dialog-text").html(text);
                        $("#bootstrap-confirm-dialog").modal("show");
                        $('#bootstrap-confirm-deposit-dialog-btn').hide();


                        
                    } else {
                        addError("#tenureFormE","#termtenureE",depositeMsg,true);
                    }
                } else {
                    addError("#tenureFormE","#termtenureE",depositeMsg,true);
                }
            }            
        }

    });

    $('#next-otp-screen').click(function(){
        $(this).prop("disabled",true);
        $('#per_verify_otp').val();
        //     $('#otp-screen-text').html(opttext);
        // //timerCountdown("#otp-screen");
        // $('#bootstrap-confirm-deposit-dialog-btn').show();
        // $('#next-otp-screen-preclouser').hide();
        // $('#otp-screen').show();
        //  $('#resendMSG').hide();
        $("#resendOTPDiv #resendMSG").removeClass("hide");
            $("#resendOTPDiv #resendBtn").addClass("hide");
            // timerCountdown("#resendOTPDiv");
            kendo.ui.progress($("body"), true);
          $.ajax({
                    url: site_url + 'dashboard/send_otp',
                    dataType: "json",
                    type: 'post',                                   
                    success: function (resultResGet) {
                        kendo.ui.progress($("body"), false);
                        $(this).prop("disabled",false);
                        $('#bootstrap-confirm-dialog-text').hide();
                        $('#bootstrap-confirm-dialog #bootstrap-confirm-dialog-heading').html("OTP Authentication");
                        $('#otp-screen').show();
                        $('#next-otp-screen').hide();
                        $('#bootstrap-confirm-deposit-dialog-btn').show();
                         timerCountdown("#otp-screen");
                            if (resultResGet['status'] == "Success") {
                                $('input[name="otp_referenceNumber"]').val(resultResGet['ReferenceNumber']);
                                // $('input[name="deposittype"]').val(deposittype);
                                // $('input[name="accountno"]').val(accountno);
                                // $('input[name="depositamount"]').val(depositamount);
                                // $('input[name="deposittenuremonths"]').val(deposittenuremonths);
                                // $('input[name="deposittenureyears"]').val(deposittenureyears);
                                // $('input[name="deposittenuredays"]').val(deposittenuredays);
                                // $('input[name="residenceoutside"]').val(residenceoutside);
                                // $('input[name="maturityoption"]').val(maturityoption);
                            }
                            else {
                                     showErrorMsg('There was an error', $("#bootstrap-confirm-dialog"));
                                 }
                        // if(checkDeleteUser(resultResGet)){
                        //     return false;
                        // }
                        // if (resultResGet['status'] == "amntError") {
                        //     showErrorMsg(resultResGet['message'], $("#fundTransferForm"));
                        // }
                        // else{
                        //         $("#step3 #resendMSG").removeClass("hide");
                        //         $("#step3 #resendBtn").addClass("hide");
                        //         timerCountdown("#bootstrap-new-deposit-dialog");
                        //         if (resultResGet['status'] == "Success") {
                        //             var beneficiaryName = $('input[name="ns22BeneficiaryName"]').val();
                        //             var beneficiaryBankIfsc = $('input[name="ns22BeneficiaryBankIfsc"]').val();
                        //             $('input[name="otp_referenceNumber"]').val(resultResGet['ReferenceNumber']);
                        //             $("#account_holder_name").text(beneficiaryName);
                        //             $("#account_holder").text(transferTo);
                        //             $("#account_holder_ifsc_code").text(beneficiaryBankIfsc);
                        //             $("#holder_transfer_amount").text(transferAmount);

                        //             $("#step2").toggle();
                        //             $("#step3").toggle();
                        //             $(".steps .list-steps li").removeClass('active');
                        //             $(".steps .list-steps li:nth-child(3)").addClass('active');
                        //         } else {
                        //             showErrorMsg('There was an error', $("#fundTransferForm"));
                        //         }
                        // }

                    },
                    error: function(xhr, status, error) {
                        if(xhr.status == 403){
                            window.location.href = site_url+"welcome/error/403";
                        }else{
                        showErrorMsg('There was an error', $("#bootstrap-new-deposit-dialog"));
                        }
                    }
                });

    });

    /****************** vicky end ******************/
   // Syam  created 
    $("#bootstrap-confirm-deposit-dialog-btn").click(function () 
    {
        // alert("Hi");      
            let verify_otp = $("#per_verify_otp").val();
            let refer_no = $("#otp_referenceNumber").val();
            let deposittype = $("#deposittype").val();
            let accountno = $("#accountno").val();
            let depositamount = $("#depositamount").val()||0;
            let deposittenuremonths = $("#deposittenuremonths").val()||0;
            let deposittenureyears = $("#deposittenureyears").val()||0;
            let deposittenuredays = $("#deposittenuredays").val()||0;
            let residenceoutside = $("#residenceoutside").val();
            let maturityoption = $("#maturityoption").val();
            // alert(verify_otp)
            if (verify_otp == "") {
                showErrorMsg('Please enter the OTP', $("#bootstrap-confirm-dialog"));
                return false;
            }
            // fd data
            let NewfdParam = {
                'verify_otp':verify_otp,
                'refer_no':refer_no,
                'deposittype': deposittype,
                'accountno': accountno,
                'depositamount': depositamount,
                'deposittenuremonths': deposittenuremonths,
                'deposittenureyears': deposittenureyears,
                'deposittenuredays': deposittenuredays,
                'residenceoutside': residenceoutside,
                'maturityoption' : maturityoption
            }
            $("#bootstrap-new-deposit-dialog").hide();
            console.log(NewfdParam);
            kendo.ui.progress($("body"), true);
            $.ajax({
                    url: site_url + 'dashboard/createDeposit',
                    dataType: "json",
                    type: 'post',
                    data: { data : encryptData(NewfdParam) },
                    beforeSend: function (res) 
                    {

                    },
                    success: function (response) {
                        kendo.ui.progress($("body"), false);
                          if(checkDeleteUser(response)){
                            return false;
                        }
                        
                        if (response['code'] != 0) {
                            //$("#bootstrap-new-deposit-dialog").modal("show");
                            showErrorMsg(response['msg'], '#bootstrap-confirm-dialog');
                        } else {
                            $("#bootstrap-confirm-dialog").modal("hide");
                            showSuccessMsg(response['msg'], '#openDeposit');
                            setTimeout(function () {
                                window.location.href = site_url + "FD_request"
                            }, 5000)
                        }
                    },
                    error: function(xhr, status, error) {
                        if(xhr.status == 403){
                            window.location.href = site_url+"welcome/error/403";
                        }else{
                            showErrorMsg(error.statusText, '#bootstrap-new-deposit-dialog');
                        }
                    }
                });

    });

    $('#bootstrap-confirm-dialog').on('hidden.bs.modal', function (e) {
        console.log("Modal hidden");
        // alert('Hi');
        // $("#bootstrap-confirm-dialog .container1").html("");
        // $("#bootstrap-confirm-dialog .container2").html("");

        $('#bootstrap-confirm-dialog-heading').html("Are you sure to proceed with the following information?");
        $('#bootstrap-confirm-dialog-text').show();
        $('#otp-screen').hide();
        $("#next-otp-screen").show();
        $("#bootstrap-confirm-deposit-dialog-btn").show();
     });

    $("#corp-user-form").validate({
        ignore: [],
        normalizer: function (value) {
            return $.trim(value);
        },
        rules: {
            corpID: {
                required: true,
                digits: true
            },
            userID: {
                required: true,
                digits: true
            },
            
           
        },
        messages: {
            corpID: {
                required: "Please enter the corporate ID"
            },
            userID: {
                required: "Please enter the user ID"
            },
            
        },
        submitHandler: function (form) {
            clearMsg(form);
           
            let user_cifid = $('#userID').val();
            let corp_cifid = $('#corpID').val();
           

            
            let addUserData = { 'corpID':corp_cifid,'userID':user_cifid}

            //showErrorMsg("Processing...", form);
            kendo.ui.progress($("body"), true);
            $.ajax({
                type: 'POST',
                // beforeSend: function () {
                //  kendo.ui.progress(form, true);
                //     // $('body').prepend("<div class='mainLoader'><div class='loader'></div></div>");
                // },
                url: site_url + 'admin/Admin/user_corp_email',
                data: { data : encryptData(addUserData) },
                dataType: 'json',
                success: function (response) {
                    console.log(response);
                    kendo.ui.progress($("body"), false);
                    if (response.code === "A"){
                        showSuccessMsg(response.status, form);
                        $('#corpID').val('');
                        $('#userID').val('');
                        
                    } else if (response.code === "B" || response.code === "c") {
                        showErrorMsg(response.status, form);
                        
                    } 
                }, 
                error: function(xhr, status, error) {
                    if(xhr.status == 403){
                        window.location.href = site_url+"welcome/error/403";
                    }else{
                        kendo.ui.progress($("body"), false);
                        showErrorMsg('Error with Code issue', form);
                    }
                }
            });            
        }
    });

    //Resend OTP Function
    $('.fdresendotp').click(function(){
        clearErrSucMsg('msg');
        clearErrSucMsg('errSucMSG');
        $('input[name="verify_otp"]').val('');
        // Get send Otp request 
        var fdotpattempt =$('#fdotpattempt').val();
        var type =$('.logintype').val();
        if(type == 'closeFD')
                {

                    var verifyOtpParam = {
                            'otpattempt': fdotpattempt,
                            'type': type,            
                        }
                }
                
                console.log(verifyOtpParam);

        kendo.ui.progress($("body"), true);
        $.ajax({
            url: site_url + 'dashboard/resend_otp',
            dataType: "json",
            type: 'post',
            data: { data : encryptData(verifyOtpParam) },
            success: function (response) {
               // kendo.ui.progress($("body"), false);
                if (response[0].code == "-1") 
                {
                    kendo.ui.progress($("body"), false);
                    showErrorMsg('Please enter the correct OTP', $('#bootstrap-confirm-dialog'));
                }
                else if (response[0].code =="-2") {
                    kendo.ui.progress($("body"), false);
                    showErrorMsg(response[0].status, $('#bootstrap-confirm-dialog'));
                } else {
                    kendo.ui.progress($("body"), false);
                    $('input[name="Fd_verify_otp"]').val('');
                    $("#resendOTPDiv #resendMSG").removeClass("hide");
                    $("#resendOTPDiv #resendBtn").addClass("hide");
                    timerCountdown("#resendOTPDiv");
                    $('input[name="otp_referenceNumber"]').val(response[0].ReferenceNumber);
                    showSuccessMsg("OTP has been resent to your mobile", $('#bootstrap-confirm-dialog'));
                }
               
                // if (resultResGet['status'] == "Success") {
                   
                //     // showSuccessMsg('#fundTransferForm', "OTP has been resent to your mobile");
                // } else {
                //     showErrorMsg("OTP has been resent to your mobile", $('#bootstrap-confirm-dialog'));
                // }

            },
            error: function(xhr, status, error) {
                if(xhr.status == 403){
                    window.location.href = site_url+"welcome/error/403";
                }else{
                showErrMsg('errSucMSG', messages.ajaxerror.en);
                }
            }
        });
    });
});
// Create Syam

   

/********************Added function by Adhir****************/

// To use this function, you have to set 'ignore: []' in jquery validator
function dropdownValidation (name, type, msgName) {
	// alert('working');
	let dropdown = $("#"+name).data("kendoDropDown"+type);
	if (dropdown) {
    $.validator.addMethod(
		name,
		function () {
			return dropdown.value().length > 0
			// return false
		},
		'Please Select '+msgName
		);
	dropdown.bind("change", function(){
		if(dropdown.value().length > 0){
			$("#" +name+ "-error").addClass("hide");
		}else{
			$("#" +name+ "-error").removeClass("hide");
		}
	})
	}
	};

/********************Added function by Neeraj****************/

function arraysEqual(_arr1, _arr2) {

    if (!Array.isArray(_arr1) || !Array.isArray(_arr2) || _arr1.length !== _arr2.length)
        return false;

    var arr1 = _arr1.concat().sort();
    var arr2 = _arr2.concat().sort();

    for (var i = 0; i < arr1.length; i++) {

        if (arr1[i] !== arr2[i])
            return false;

    }

    return true;

}

/********************Added function by Gerard*****************/

// Clear All Messages
function clearMsg(form) {
    $(form).find(".msg").removeClass("alert-success");
    $(form).find(".msg").removeClass("alert-danger");
    $(form).find(".msg").text('');
    $(form).find(".msg1").removeClass("alert-success");
    $(form).find(".msg1").removeClass("alert-danger");
    $(form).find(".msg1").removeClass("alert");
    $(form).find(".msg1").html('');
}

// Display Success Message
function showSuccessMsg(msg, form,hide = true) {
    clearMsg(form);
    $(form).find(".msg").addClass("alert-success");
    //$(form).find(".msg").css({"backgroundColor":"#0e9605","color":"white"});
	$(form).find(".msg").text(msg);
    if(hide){
        setTimeout(function () {
            clearMsg(form)
        },3000);
    }
}

// Display Error Message
function showErrorMsg(msg, form,hide = true) {
    clearMsg(form);
    $(form).find(".msg").addClass("alert-danger");
	$(form).find(".msg").text(msg);
    if(hide){
        setTimeout(function () {
            clearMsg(form);
        },5000);
    }
}


/********************Added function by Ali*****************/

//Confirm fund transfer
function confirmFundTrsfer() 
{
    var transferType = $('select#transfer_type option:selected').val();
    let transferFrom = $("#transfer_from").data("kendoDropDownTree").value();
    let transferTo = $("#transfer_to").data("kendoDropDownTree").value();
    var transferAmount = $('input[name="transfer_amount"]').val();

    if (transferAmount == "") {
        showErrorMsg('Please enter the transfer amount', $("#fundTransferForm"));
        return false;
    }
    if (!$.isNumeric(transferAmount)) {
        showErrorMsg('Please enter numbers only', $("#fundTransferForm"));
        return false;
    }

    if (transferType != "" && transferTo != "" && transferAmount != "" &&  $("#step3Flag").val() != 0) {
        // Get send Otp request 
        $("#account_holder_name").text('');
        $("#account_holder").text('');
        $("#account_holder_ifsc_code").text('');
        $("#holder_transfer_amount").text('');
        
        let acctTxnParam = {
            'transferType': transferType,
            'transferFrom': transferFrom,
            'transferTo': transferTo,
            'transferAmount': transferAmount
        }
        
        kendo.ui.progress($("body"), true);
        $.ajax({
            url: site_url + 'dashboard/send_otp',
            dataType: "json",
            type: 'post',
            data: { data : encryptData(acctTxnParam) },
            success: function (resultResGet) {
                kendo.ui.progress($("body"), false);
                if(checkDeleteUser(resultResGet)){
                    return false;
                }
                if (resultResGet['status'] == "amntError") {
                    showErrorMsg(resultResGet['message'], $("#fundTransferForm"));
                }else{
                        $("#step3 #resendMSG").removeClass("hide");
                        $("#step3 #resendBtn").addClass("hide");
                        timerCountdown("#step3");
                        if (resultResGet['status'] == "Success") {
                            var beneficiaryName = $('input[name="ns22BeneficiaryName"]').val();
                            var beneficiaryBankIfsc = $('input[name="ns22BeneficiaryBankIfsc"]').val();
                            $('input[name="otp_referenceNumber"]').val(resultResGet['ReferenceNumber']);
                            if(beneficiaryName =='')
                            {
                                $("#account_holder_name").text(beneficiaryName);
                            }
                            else
                            {

                            $("#account_holder_name").text("Transfer To: "+beneficiaryName);
                            }
                           // $("#account_holder_name").text(beneficiaryName);
                            $("#account_holder").text(transferTo);
                            $("#account_holder_ifsc_code").text(beneficiaryBankIfsc);
                            $("#holder_transfer_amount").text(formatToInr(transferAmount));

                            $("#step2").toggle();
                            $("#step3").toggle();
                            $(".steps .list-steps li").removeClass('active');
                            $(".steps .list-steps li:nth-child(3)").addClass('active');
                        } else {
                            showErrorMsg('There was an error', $("#fundTransferForm"));
                        }
                }

            },
            error: function(xhr, status, error) {
                if(xhr.status == 403){
                    window.location.href = site_url+"welcome/error/403";
                }else{
                showErrorMsg('There was an error', $("#fundTransferForm"));
                }
            }
        });


    }
}

//verify otp function
function verifyOtp() {

    var verifyOtp = $('input[name="verify_otp"]').val();
    var otpReferenceNo = $('input[name="otp_referenceNumber"]').val();
    var transferType = $('select#transfer_type option:selected').val();
    let transferFrom = $("#transfer_from").data("kendoDropDownTree").value();
    let transferTo = $("#transfer_to").data("kendoDropDownTree").value();
    var transferAmount = $('input[name="transfer_amount"]').val();
    var beneficiaryName = $('input[name="ns22BeneficiaryName"]').val();
    var beneficiaryBankIfsc = $('input[name="ns22BeneficiaryBankIfsc"]').val();
    var ns22Sequence = $('input[name="ns22Sequence"]').val();
    var naration = $('#naration').val();

    if (verifyOtp == "") {
        showErrorMsg('Please enter the OTP', $("#fundTransferForm"));
        return false;
    }
    if (verifyOtp != "" && otpReferenceNo != "") {
        // Get verify Otp request 
        let verifyOtpParam = {
            'verifyOtp': verifyOtp,
            'otpRefNo': otpReferenceNo,
            'transferType': transferType,
            'transferTo': transferTo,
            'transferFrom': transferFrom,
            'transferAmount': transferAmount,
            'beneficiaryName': beneficiaryName,
            'beneficiaryBankIfsc': beneficiaryBankIfsc,
            'ns22Sequence'  :ns22Sequence,
            'naration' : naration
        }
        console.log(verifyOtpParam);
        kendo.ui.progress($("body"), true);
        $.ajax({
            url: site_url + 'dashboard/verify_otp',
            dataType: "json",
            type: 'post',
            data: { data : encryptData(verifyOtpParam) },
            success: function (response) {
                kendo.ui.progress($("body"), false);
                if(checkDeleteUser(response)){
                    return false;
                }
                // console.log(response);
                if (response.status == "failed") {
                    showErrorMsg('Please enter the correct OTP', $("#fundTransferForm"));
                } else if (response[0].code != 0) {
                    showErrorMsg(messages.ajaxerror.en,  $("#fundTransferForm"));
                } else {
                    resetFundTransferData();
                    showSuccessMsg(response[0].status, $("#fundTransferForm"));
                    
                    $("#step1").toggle();
                    $("#step3").toggle();
                    $(".steps .list-steps li").removeClass('active');
                    $(".steps .list-steps li:nth-child(1)").addClass('active');
                }


            },
            error: function(xhr, status, error) {
                if(xhr.status == 403){
                    window.location.href = site_url+"welcome/error/403";
                }else{
                showErrorMsg(messages.ajaxerror.en,  $("#fundTransferForm"));
                }
            }
        });

    }
}






// Clear All Messages
function clearErrSucMsg(errSucMSG) {
    $("." + errSucMSG).removeClass("alert alert-success");
    $("." + errSucMSG).removeClass("alert alert-danger");
    $("." + errSucMSG).text('');
}

// Display Success Message
function showSucMsg(errSucMSG, msg,hide = true) {
    clearErrSucMsg(errSucMSG);
    $("." + errSucMSG).fadeIn("slow");
    $("." + errSucMSG).addClass("alert alert-success");
    $("." + errSucMSG).html(msg);
    if(hide){
        setTimeout(function () {
            $("." + errSucMSG).fadeOut("slow");
        },5000);
    }
}

// Display Error Message
function showErrMsg(errSucMSG, msg,hide = true) {

    clearErrSucMsg(errSucMSG);
    $("." + errSucMSG).fadeIn("slow");
    $("." + errSucMSG).addClass("alert alert-danger");
    $("." + errSucMSG).html(msg);
    if(hide){
        setTimeout(function () {
            $("." + errSucMSG).fadeOut("slow");
        },5000);
    }
}

function resetFundTransferData() {
    $('input[name="ns22BeneficiaryName"]').val('');
    $('input[name="ns22BeneficiaryBankIfsc"]').val('');
    $('input[name="otp_referenceNumber"]').val('');
    $('input[name="transfer_amount"]').val('');
    $('input[name="verify_otp"]').val('');
    $("#acc_holder_name").text('');
    $("#acc_holder").text('');
    $("#acc_holder_ifsc_code").text('');
    $("#account_holder_name").text('');
    $("#account_holder").text('');
    $("#account_holder_ifsc_code").text('');
    $("#holder_transfer_amount").text('');

    $("#demo4 input").val("");
    $("#demo4 textarea").val("");
    $("#demo4 select").val("").trigger("change");
    $("#demo4 select").data("kendoDropDownList").value("");
    $("#amtE").text("");

}

function addPayeeID() {
    $("#addPayeeForm").submit();
}




//select ifsc code, and function
function selectTransAcc() {

    let transferType = $('select#transfer_type').val();
    let transferFrom = $("#transfer_from").data("kendoDropDownTree").value();
	let transferTo = $("#transfer_to").data("kendoDropDownTree").value();
	let form = $("#fundTransferForm");

    clearErrSucMsg('errSucMSG');
    if (transferType == "") {
		showErrorMsg("Select Transfer Type", form);
        // showErrMsg('errSucMSG', 'Please select transfer!.');
        // return false;
    }else if (transferFrom == "") {
		showErrorMsg("Select Transfer From Account", form);
        // showErrMsg('errSucMSG', 'Please select transfer from account!.');
        return false;holder_transfer_amount
    }else if (transferTo == "") {
		// showErrMsg('errSucMSG', 'Please select transfer to account!.');
		showErrorMsg("Select Transfer To Account", form);
        return false;
    }

    if (transferType != "" && transferFrom != "" && transferTo != "") {

		clearMsg(form);
        //kendo.ui.progress($("body"), true);
        $('#balance-data').modal("show");
        // Get Account Name and IFSC Code request 
        

    }
}
function nextOpen()
{
    let transferType = $('select#transfer_type').val();
    let transferFrom = $("#transfer_from").data("kendoDropDownTree").value();
    let transferTo = $("#transfer_to").data("kendoDropDownTree").value();
    let form = $("#fundTransferForm");
    $('#balance-data').modal("hide");
    kendo.ui.progress($("body"), true);
    clearErrSucMsg('errSucMSG');
    let acctTranToParam = {
            'transferType': transferType,
            'transferFrom': transferFrom,
            'transferTo': transferTo
        }
        $('input[name="ns22BeneficiaryName"]').val('');
        $('input[name="ns22BeneficiaryBankIfsc"]').val('');
        $("#acc_holder_name").text('');
        $("#acc_holder").text('');
        $("#acc_holder_ifsc_code").text('');
        
        
        $.ajax({
            url: site_url + '/dashboard/get_acc_ifsc_code',
            dataType: "json",
            type: 'post',
            data: { data : encryptData(acctTranToParam) },
            success: function (resultResGet) {
                kendo.ui.progress($("body"), false);
                if(checkDeleteUser(resultResGet)){
                    return false;
                }
                if (resultResGet['code'] == '0') {
                    var beneficiaryName = resultResGet['status']['ns22BeneficiaryName'];
                    var beneficiaryBankIfsc = resultResGet['status']['ns22BeneficiaryBankIfsc'];
                    var ns22Sequence = resultResGet['status']['ns22Sequence'];
                    $('input[name="ns22BeneficiaryName"]').val(beneficiaryName);
                    $('input[name="ns22BeneficiaryBankIfsc"]').val(beneficiaryBankIfsc);
                    $('input[name="ns22Sequence"]').val(ns22Sequence);
                    //alert(beneficiaryName);
                    if(beneficiaryName =='')
                    {
                        $("#acc_holder_name").text(beneficiaryName);
                    }
                    else
                    {

                    $("#acc_holder_name").text("Transfer To: "+beneficiaryName);
                    }
                    $("#acc_holder").text(transferTo);
                    $("#acc_holder_ifsc_code").text(beneficiaryBankIfsc);
                    $("#step1").toggle();
                    $("#step2").toggle();
                    $(".steps .list-steps li").removeClass('active');
                    $(".steps .list-steps li:nth-child(2)").addClass('active');
                } else {
                    showErrorMsg(resultResGet['status'], form);
                    // showErrMsg('errSucMSG', resultResGet['message']);
                }

            },
            error: function(xhr, status, error) {
                if(xhr.status == 403){
                    window.location.href = site_url+"welcome/error/403";
                }else{
                    showErrorMsg("There was an error", form);
                }
            }
        });

}
function sendOTPPayee(){

    
}

function getFtfromData(sel) 
{

    var selectedID = sel.value;
    //alert("selectedID"+selectedID);
	var selectedText = $(sel).find("option:selected").text();
    //alert("selectedText"+selectedText);
	$('#transfer_to').data("kendoDropDownTree").value('')
	$('#transfer_to').data("kendoDropDownTree").enable(false)
	$('#transfer_from').data("kendoDropDownTree").value('')
	$('#transfer_from').data("kendoDropDownTree").enable(false)
	// $('#transfer_to').data("kendoDropDownTree").setOptions({placeholder: 'Loading...'})


    if (selectedID != "" && selectedText != "") {
		// alert('ali dropdown');
        // Get Account Name and IFSC Code request 
        let ftFromParam = {
            'selectedID': selectedID,
            'selectedText': selectedText
        }
        var selPlaceHolder = "Loading..";
        $('#transfer_from').kendoDropDownTree({
            placeholder: selPlaceHolder,
			dataSource: [],
			filter: "contains"
        });
        // if(selectedID == 'I')
        // {
        //     let requestParam = {
        //             'transferType': selectedID,
                   
        //         }
               
        //         $('#transfer_from').data("kendoDropDownTree").value('');
        //         $('#transfer_from').data("kendoDropDownTree").enable(false);
        //     $.ajax({
        //         type: 'POST',
        //         url: site_url  + 'dashboard/corp_list',
        //         data: { data : encryptData(requestParam) },
        //         dataType: 'json',
        //         success: function (result) {
        //             kendo.ui.progress($("body"), false);
                    
        //             if(checkDeleteUser(result)){
        //                 return false;
        //             }
        //             if(result[0]['corpMobile'] == '' || result[0]['corpMobile'] == null )
        //             {

        //                 kendo.alert('Please update your mobile number');
        //               //fundTransferDropDown.setDataSource(result);
        //                 $('#transfer_from').data("kendoDropDownTree").enable(false);
        //                 $('#transfer_from').data("kendoDropDownTree").setOptions({placeholder: '---No Accounts Found---'});
        //             }
        //             else
        //             {
        //                 ajaxRequestForDropDown(ftFromParam, "transfer_from", "ftFromData");
        //             }
        //             // if (result.code) {
        //             //     showErrorMsg(result.status, $("#fundTransferForm"));
        //             //     fundTransferDropDown.setOptions({placeholder: '--Select Transfer From--'})
        //             // } else {
        //             //     if (typeof result !== 'undefined' && result.length > 0){
        //             //         fundTransferDropDown.setDataSource(result);
        //             //         fundTransferDropDown.list.width("auto");
        //             //         fundTransferDropDown.enable(true)
        //             //         if (callFuncDetails == "ftFromData") {
        //             //             fundTransferDropDown.bind("change", getFtToData)
        //             //             fundTransferDropDown.setOptions({placeholder: '---Select Transfer From---'})
        //             //         } else {
        //             //             fundTransferDropDown.setOptions({placeholder: '---Select Transfer To---'})
        //             //         }
        //             //     } else {
        //             //         fundTransferDropDown.setOptions({placeholder: '---No Accounts Found---'})
        //             //     }
        //             // }
        //         },
        //         error: function(xhr, status, error) {

        //             expiresession(xhr, status, error);
        //         //    if(xhr.status == 403){
        //         //        window.location.href = site_url+"welcome/error/403";
        //         //    }
        //         }
        //     });
        // }
        // else
        // {
            
        //     ajaxRequestForDropDown(ftFromParam, "transfer_from", "ftFromData");
        // }

        // $('#transfer_to').kendoDropDownTree({
        //     placeholder: "test",
		// 	dataSource: [],
		// 	filter: "contains"
		// });
          // $("#dialog").kendoDialog({
          //     title: "Kendo Dialog Component",
          //     content: "This is your Kendo Dialog.",
          //     buttonLayout: "normal",
          //     closable:true,
          //     modal:true,
          //     actions: [{
          //         text: "OK",
          //         action: function(e){
          //             console.log('user ok');
          //             return true;
          //         },
          //         primary: true
          //     }]
          //   });
		

        ajaxRequestForDropDown(ftFromParam, "transfer_from", "ftFromData");
    }
}



function getFtToData() {

	var transTypeID = $("#transfer_type").find("option:selected").val();
	// var testID = $("#transfer_type").value();
	
    var selectedAccID = $("#transfer_from").data("kendoDropDownTree").value();
    if (transTypeID != "" && selectedAccID != "") {
        // Get Account Name and IFSC Code request 
        let ftToParam = {
            'transTypeID': transTypeID,
            'selectedAccID': selectedAccID
        }
        var selPlaceHolder = "---Select transfers to --- ";
        $('#transfer_to').kendoDropDownTree({
            placeholder: selPlaceHolder,
			dataSource: [],
			filter: "contains"
        });
        ajaxRequestForDropDown(ftToParam, "transfer_to", "ftToData");

    }

}

function ajaxRequestForDropDown(requestParam, dropDownID, callFuncDetails) {
	clearMsg($("#fundTransferForm"))
	$('#' + dropDownID).html('');
	let fundTransferDropDown = $('#' + dropDownID).data("kendoDropDownTree")
	fundTransferDropDown.setOptions({placeholder: 'Loading...'})
	fundTransferDropDown.enable(false)
    kendo.ui.progress($("body"), true);
    $.ajax({
		type: 'POST',
        url: site_url  + callFuncDetails,
        data: { data : encryptData(requestParam) },
        dataType: 'json',
        success: function (result) {
            kendo.ui.progress($("body"), false);
            
            if(checkDeleteUser(result)){
                return false;
            }

			if (result.code) {
				showErrorMsg(result.status, $("#fundTransferForm"));
				fundTransferDropDown.setOptions({placeholder: '--Select Transfer From--'})
			} else {
				if (typeof result !== 'undefined' && result.length > 0){
					fundTransferDropDown.setDataSource(result);
					fundTransferDropDown.list.width("auto");
					fundTransferDropDown.enable(true)
					if (callFuncDetails == "ftFromData") {
						fundTransferDropDown.bind("change", getFtToData)
						fundTransferDropDown.setOptions({placeholder: '---Select Transfer From---'})
					} else {
						fundTransferDropDown.setOptions({placeholder: '---Select Transfer To---'})
					}
				} else {
					fundTransferDropDown.setOptions({placeholder: '---No Accounts Found---'})
				}
			}
		},
        error: function(xhr, status, error) {

            expiresession(xhr, status, error);
        //    if(xhr.status == 403){
        //        window.location.href = site_url+"welcome/error/403";
        //    }
        }
    });
}

function expiresession(xhr, status, error){
  
    if(xhr.status == 403){
        window.location.href = site_url+"welcome/error/403";
    }else if(xhr.status == 502){
        window.location.href = site_url+"welcome/error/502";
    }else{
        showErrorMsg(messages.ajaxerror.en, form);
    }
}

function groupSelectFieldReqORNot(selID){
    if ($("#"+selID).val() !="") {
        return true
    } else {
        return false
    }
}

function checkDeleteUser(result){
    
    if(result['code'] == '203'){
        kendo.alert(result['status']);
        return true;
    }else{
        return  false;
    }

}

// login resend otp
    function init() {
        let count = localStorage.getItem('counter');
        if(count === null){
            count = 1;
            localStorage.setItem('counter', count);
        }
        else
        {
            count = 1;
            localStorage.setItem('counter', count);
            updateCount(count);
        }
    }
    function incrementCounter() {
        let count = parseInt(localStorage.getItem('counter'));
        count = count + 1;
        localStorage.setItem('counter', count);
        updateCount(count);
        return true;
    }
    function updateCount(count) {
       
        document.getElementById("otpattempt").value = count;

    }
// forget resend otp
    function init1() {
        let count1 = localStorage.getItem('counter1');
        if(count1 === null){
            count1 = 1;
            localStorage.setItem('counter1', count1);
        }
        else
        {
            count1 = 1;
            localStorage.setItem('counter1', count1);
            updateCount(count1);
        }
        // count1 = parseInt(count1);
        // updateCount1(count1);
    }
    function ForgetCounter() {
        let count1 = parseInt(localStorage.getItem('counter1'));
        count1 = count1 + 1;
        localStorage.setItem('counter1', count1);
        updateCount1(count1);
        return true;
    }
    function updateCount1(count1) {
      
       document.getElementById("forgotattempt").value = count1;
    }
    // bank login send otp
    function init2() {
        let count2 = localStorage.getItem('counter2');
        if(count2 === null){
            count2 = 1;
            localStorage.setItem('counter2', count2);
        }
        else
        {
            count2 = 1;
            localStorage.setItem('counter2', count2);
            updateCount2(count2);
        }
        // count1 = parseInt(count1);
        // updateCount1(count1);
    }
    function BankCounter() {
        let count2 = parseInt(localStorage.getItem('counter2'));
        count2 = count2 + 1;
        localStorage.setItem('counter2', count2);
        updateCount2(count2);
        return true;
    }
    function updateCount2(count2) {
      
       document.getElementById("Bankotpattempt").value = count2;
    }
    // super login resend otp
    function init3() {
        let count3 = localStorage.getItem('counter3');
        //alert(count3);
        if(count3 === null){
            count3 = 1;
            localStorage.setItem('counter3', count3);
        }
        else
        {
            count3 = 1;
            localStorage.setItem('counter3', count3);
            updateCount3(count3);
        }
        // count1 = parseInt(count1);
        // updateCount1(count1);
    }
    function SuperCounter() {
        let count3 = parseInt(localStorage.getItem('counter3'));
        count3 = count3 + 1;
        localStorage.setItem('counter3', count3);
        updateCount3(count3);
        return true;
    }
    function updateCount3(count3) 
    {      
       // alert("otp:"+count3);
        $('#superotpattempt').val(count3);
      // document.getElementById("Superotpattempt").value = count3;
    }

    //FT resend otp
    function init4() {
        let count4 = localStorage.getItem('counter4');
        //alert(count3);
        if(count4 === null){
            count4 = 1;
            localStorage.setItem('counter4', count4);
        }
        else
        {
            count4 = 1;
            localStorage.setItem('counter4', count4);
            updateCount4(count4);
        }
        // count1 = parseInt(count1);
        // updateCount1(count1);
    }
    function ftCounter() {
        let count4 = parseInt(localStorage.getItem('counter4'));
        count4 = count4 + 1;
        localStorage.setItem('counter4', count4);
        updateCount4(count4);
        return true;
    }
    function updateCount4(count4) 
    {      
       // alert("otp:"+count3);
        $('#ftotpattempt').val(count4);
      // document.getElementById("Superotpattempt").value = count3;
    }

    //CloseFD resend otp
    function init5() {
        let count5 = localStorage.getItem('counter5');
        //alert(count3);
        if(count5 === null){
            count5 = 1;
            localStorage.setItem('counter5', count5);
        }
        else
        {
            count5 = 1;
            localStorage.setItem('counter5', count5);
            updateCount5(count5);
        }
        // count1 = parseInt(count1);
        // updateCount1(count1);
    }
    function closeFDCounter() {
        let count5 = parseInt(localStorage.getItem('counter5'));
        count5 = count5 + 1;
        localStorage.setItem('counter5', count5);
        updateCount5(count5);
        return true;
    }
    function updateCount5(count5) 
    {      
       // alert("otp:"+count3);
        $('#closefdotpattempt').val(count5);
      // document.getElementById("Superotpattempt").value = count3;
    }
    // FD resend otp
    function init6() {
        let count6 = localStorage.getItem('counter6');
        //alert(count3);
        if(count6 === null){
            count6 = 1;
            localStorage.setItem('counter6', count6);
        }
        else
        {
            count6 = 1;
            localStorage.setItem('counter6', count6);
            updateCount6(count6);
        }
        // count1 = parseInt(count1);
        // updateCount1(count1);
    }
    function fdCounter() {
        let count6 = parseInt(localStorage.getItem('counter6'));
        count6 = count6 + 1;
        localStorage.setItem('counter6', count6);
        updateCount6(count6);
        return true;
    }
    function updateCount6(count6) 
    {      
       // alert("otp:"+count3);
        $('#fdotpattempt').val(count6);
      // document.getElementById("Superotpattempt").value = count3;
    }